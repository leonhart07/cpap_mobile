#!/bin/sh
# This is a generated file; do not edit or check into version control.
export "FLUTTER_ROOT=C:\src\flutter"
export "FLUTTER_APPLICATION_PATH=D:\working\cpap"
export "FLUTTER_BUILD_DIR=build"
export "FLUTTER_FRAMEWORK_DIR=C:\src\flutter\bin\cache\artifacts\engine\darwin-x64"
export "FLUTTER_BUILD_NAME=1.0.4"
export "FLUTTER_BUILD_NUMBER=29"
export "DART_OBFUSCATION=false"
export "TRACK_WIDGET_CREATION=false"
export "TREE_SHAKE_ICONS=false"
export "PACKAGE_CONFIG=.packages"
