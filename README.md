# CPAP MOBILE APP #


### FEATURE ###
1. Login
2. View patient
3. Receive push notification


### NEEDED ###

### ISSUE TRACKER ###
https://cpapvn.backlog.com/dashboard

### HOW TO COMPILE ###
1. Install android SDK : https://developer.android.com/studio
2. Install flutter SDK : https://flutter.dev/docs/get-started/install/windows
3. Editor : install VSCode with dart and flutter plugin : https://code.visualstudio.com/
4. Run : go to project folder and run command `flutter run`


### RELEASE GUIDELINE ###
