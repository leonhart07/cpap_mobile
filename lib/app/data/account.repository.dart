import 'package:cpap_mobile/app/data/sources/remote/http.account.service.dart';
import 'package:cpap_mobile/app/data/structure/common.dart';
import 'package:cpap_mobile/app/data/sources/remote/common/http_response.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:cpap_mobile/device/connection/connection.helper.dart';

class AccountRepository {
  HTTPAccountService service = inject<HTTPAccountService>();

  Future<List<dynamic>> getAccounts() async {
    HttpResponse response = HttpResponse();

    final hasConnection = await ConnectionHelper.hasConnection();

    if (hasConnection) {
      response = await this.service.getAllAccounts();
    } else {
      response.message = "Device offline";
    }

    List<dynamic> results = [];
    if (response.statusCode == 200) {
      Map<String, dynamic> jsonResponse = response.data;
      if (jsonResponse.containsKey("data")) {
        final list = jsonResponse['data']['accounts'] as List;
        if (list != null) {
          results = list;
        }
      }
    }
    return results;
  }

  Future<bool> newAccount(Account account) async {
    HttpResponse response = HttpResponse();

    final hasConnection = await ConnectionHelper.hasConnection();

    if (hasConnection) {
      response = await this.service.newAccount(account);
    } else {
      response.message = "Device offline";
    }
    return response.statusCode == 200;
  }

  Future<bool> deleteAccount(String accId) async {
    HttpResponse response = HttpResponse();

    final hasConnection = await ConnectionHelper.hasConnection();

    if (hasConnection) {
      response = await this.service.deleteAccount(accId);
    } else {
      response.message = "Device offline";
    }
    return response.statusCode == 200;
  }

  Future<bool> editAccount(Account account) async {
    HttpResponse response = HttpResponse();

    final hasConnection = await ConnectionHelper.hasConnection();

    if (hasConnection) {
      response = await this.service.editAccount(account);
    } else {
      response.message = "Device offline";
    }
    return response.statusCode == 200;
  }
}
