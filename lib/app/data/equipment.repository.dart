import 'package:cpap_mobile/app/data/structure/common.dart';
import 'package:cpap_mobile/app/data/sources/remote/http.equipment.service.dart';
import 'package:cpap_mobile/app/data/sources/remote/common/http_response.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:cpap_mobile/device/connection/connection.helper.dart';

class EquipmentRepository {
  HTTPEquipmentService service = inject<HTTPEquipmentService>();

  Future<List<dynamic>> getEquipments() async {
    HttpResponse response = HttpResponse();

    final hasConnection = await ConnectionHelper.hasConnection();

    if (hasConnection) {
      response = await this.service.getAllEquipments();
    } else {
      response.message = "Device offline";
    }

    List<dynamic> results = [];
    if (response.statusCode == 200) {
      Map<String, dynamic> jsonResponse = response.data;
      if (jsonResponse.containsKey("data")) {
        final list = jsonResponse['data']['equipments'] as List;
        if (list != null) {
          results = list;
        }
      }
    }
    return results;
  }

  Future<bool> addEquipment(Equipment equipment) async {
    HttpResponse response = HttpResponse();

    final hasConnection = await ConnectionHelper.hasConnection();

    if (hasConnection) {
      response = await this.service.newEquipment(equipment.name);
    } else {
      response.message = "Device offline";
    }
    return response.statusCode == 200;
  }

  Future<bool> deleteEquipment(String equipmentId) async {
    HttpResponse response = HttpResponse();

    final hasConnection = await ConnectionHelper.hasConnection();

    if (hasConnection) {
      response = await this.service.deleteEquipment(equipmentId);
    } else {
      response.message = "Device offline";
    }
    return response.statusCode == 200;
  }

  Future<bool> editEquipmentName(equipment) async {
    HttpResponse response = HttpResponse();

    final hasConnection = await ConnectionHelper.hasConnection();

    if (hasConnection) {
      response = await this.service.editEquipmentInfo(equipment);
    } else {
      response.message = "Device offline";
    }
    return response.statusCode == 200;
  }

  Future<bool> editEquipmentBooking(equipment) async {
    HttpResponse response = HttpResponse();

    final hasConnection = await ConnectionHelper.hasConnection();

    if (hasConnection) {
      response = await this.service.editEquipmentBooking(equipment);
    } else {
      response.message = "Device offline";
    }
    return response.statusCode == 200;
  }
}
