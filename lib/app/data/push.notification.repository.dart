import 'package:cpap_mobile/app/data/sources/remote/http.push.notification.service.dart';
import 'package:cpap_mobile/app/data/sources/remote/common/http_response.dart';
import 'package:cpap_mobile/app/data/structure/common.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:cpap_mobile/device/connection/connection.helper.dart';

class PushNotificationRepository {
  HTTPNotificationService service = inject<HTTPNotificationService>();

  Future<void> sub(String fcmToken) async {
    final hasConnection = await ConnectionHelper.hasConnection();

    if (hasConnection) {
      final sub = await this.service.sub(fcmToken);
      if (sub) {
        print('sub push notification success!!!');
      } else {
        print('sub push notification failed!!!');
      }
    } else {
      // no connection
    }
  }

  Future<void> unsub(String fcmToken) async {
    final hasConnection = await ConnectionHelper.hasConnection();

    if (hasConnection) {
      final sub = await this.service.unsub(fcmToken);
      if (sub) {
        print('unsub push notification success!!!');
      } else {
        print('unsub push notification failed!!!');
      }
    } else {
      // no connection
    }
  }

  Future<Map<String, dynamic>> getNotifications(int pageNo) async {
    HttpResponse response = HttpResponse();

    final hasConnection = await ConnectionHelper.hasConnection();

    if (hasConnection) {
      response = await this.service.getNotifications(pageNo);
    } else {
      response.message = "Not Found";
    }

    if (response.statusCode == 200) {
      Map<String, dynamic> jsonResponse = response.data;
      if (jsonResponse.containsKey("data")) {
        return jsonResponse['data'];
      }
    }
    return null;
  }

  Future<bool> addNotification(NotificationPost dataPost) async {
    HttpResponse response = HttpResponse();

    final hasConnection = await ConnectionHelper.hasConnection();

    if (hasConnection) {
      response = await this.service.addNotification(dataPost);
    } else {
      response.message = "Device offline";
    }
    return response.statusCode == 200;
  }
}
