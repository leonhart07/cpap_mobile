class Account {
  String id = '';
  String email = '';
  String password = '';
  String name = '';
  String phone = '';
  String address = '';
  String hospital = '';
  String birthday = '';
  String clinic = '';
  String role = '';
}

class Equipment {
  String name = '';
}

class Role {
  String name;
  String value;
  Role({this.name, this.value});
}

class Patient {
  String address = '';
  String birthday = '';
  String doctorId = '';
  String email = '';
  String fullname = '';
  String phone = '';
}

class Login {
  String email = '';
  String password = '';
}

class ChangePassword {
  String oldPassword = '';
  String newPassword = '';
}

class UserProfile {
  final String authenToken;
  final String role;
  final String fullname;
  final String name;
  final dynamic age;
  final String birthday;
  final String addressStr;
  final String phoneNumber;
  final String email;
  final String id;
  final String hospitalName;
  final String hospitalFaculty;
  final String clinic;

  UserProfile(
      this.authenToken,
      this.role,
      this.fullname,
      this.name,
      this.age,
      this.birthday,
      this.addressStr,
      this.phoneNumber,
      this.email,
      this.id,
      this.hospitalName,
      this.hospitalFaculty,
      this.clinic);

  static UserProfile fromJson(Map<String, dynamic> json) {
    return UserProfile(
        json['authen_token'],
        json['role'],
        json["fullname"],
        json["name"] as String,
        json["age"] as dynamic,
        json["birthday"] as String,
        json["addressStr"] as String,
        json["phone_number"] as String,
        json["email"] as String,
        json["id"] as String,
        json["hospital_name"] as String,
        json["hospital_faculty"] as String,
        json["clinic"]);
  }
}

class NotificationPost {
  String body;
  String image;
  String title;
  List<String> accountID;

  NotificationPost({
    this.body = '',
    this.image = '',
    this.title = '',
    this.accountID,
  });
}
