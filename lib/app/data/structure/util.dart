import 'dart:convert';
import 'package:crypto/crypto.dart';

typedef FetchDataDoneCallBack = void Function(dynamic data);
typedef FetchDataRemoteCall = void Function(int value,
    {FetchDataDoneCallBack callback});

String validateEmail(String email) {
  RegExp regex = RegExp(
      r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$");
  if (email.isEmpty || !regex.hasMatch(email)) return 'Email không hợp lệ';
  return null;
}

String validatePassword(String password) {
  // RegExp regex =
  //     RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$');
  RegExp regex = RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])');
  if (password.isEmpty || !regex.hasMatch(password))
    return 'Mật khẩu phải có chữ thường/chữ hoa/số';
  else
    return null;
}

int dayDifference(DateTime from, DateTime to) {
  return DateTime(from.year, from.month, from.day)
      .difference(DateTime(to.year, to.month, to.day))
      .inDays;
}

DateTime stringIsoToLocalDateTime(String value) {
  var dtVal = DateTime.parse(value ?? '2021-03-05T00:00:00.000000');
  var dtLocal = dtVal.toLocal();

  if (dtVal == dtLocal) {
    DateTime dateTime = DateTime.now();
    var timeDiff = new Duration(
        hours: dateTime.timeZoneOffset.inHours,
        minutes: dateTime.timeZoneOffset.inMinutes % 60);
    return dtVal.add(timeDiff);
  } else
    return dtLocal;
}

String generateMd5(String input) {
  return md5.convert(utf8.encode(input)).toString();
}
