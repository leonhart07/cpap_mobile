import 'package:cpap_mobile/app/data/structure/common.dart';
import 'package:cpap_mobile/app/data/sources/remote/http.user.service.dart';
import 'package:cpap_mobile/app/data/sources/remote/common/http_response.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:cpap_mobile/device/connection/connection.helper.dart';

class UserRepository {
  HTTPUserService service = inject<HTTPUserService>();

  // ==========================================================
  //                   USER LOGIN
  // ==========================================================

  Future<HttpResponse> login(String login, String password) async {
    HttpResponse response = HttpResponse();

    final hasConnection = await ConnectionHelper.hasConnection();

    if (hasConnection) {
      response = await this.service.login(login, password);
    } else {
      response.message = "Device offline";
    }

    return response;
  }

  Future<HttpResponse> checkToken(String token) async {
    HttpResponse response = HttpResponse();

    final hasConnection = await ConnectionHelper.hasConnection();

    if (hasConnection) {
      response = await this.service.loginWithToken(token);
    } else {
      response.message = "Device offline";
    }

    return response;
  }

  // ==========================================================
  //                   USER PASSWORD
  // ==========================================================

  Future<HttpResponse> forgotPassword(String email) async {
    HttpResponse response = HttpResponse();
    final hasConnection = await ConnectionHelper.hasConnection();

    if (hasConnection) {
      response = await this.service.forgotPassword(email);
    } else {
      response.message = "Device offline";
    }

    return response;
  }

  Future<HttpResponse> changePassword(
      String oldPassword, String newPassword) async {
    final hasConnection = await ConnectionHelper.hasConnection();

    if (hasConnection) {
      final response =
          await this.service.changePassword(oldPassword, newPassword);
      if (response.statusCode == 200) {
        print('change password success!!!');
      } else {
        print('change password failed!!!');
      }

      return response;
    } else {
      // no connection
      return null;
    }
  }

  // ==========================================================
  //                   USER UTILITIES
  // ==========================================================

  Future<List<Role>> getRoles() async {
    HttpResponse response = HttpResponse();

    final hasConnection = await ConnectionHelper.hasConnection();

    if (hasConnection) {
      response = await this.service.getRoleResponse();
    } else {
      response.message = "Device offline";
    }

    List<Role> roles = [];
    if (response.statusCode == 200) {
      Map<String, dynamic> jsonResponse = response.data;
      if (jsonResponse.containsKey("data")) {
        final role = jsonResponse['data']['role'];
        final map = Map<String, dynamic>.from(role);
        if (map != null) {
          roles = map.entries
              .map((e) => Role(name: e.key, value: e.value))
              .toList();
        }
      }
    }
    return roles;
  }
}
