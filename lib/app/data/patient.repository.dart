import 'package:cpap_mobile/app/data/sources/remote/http.patient.service.dart';
import 'package:cpap_mobile/app/data/structure/common.dart';
import 'package:cpap_mobile/app/data/sources/remote/common/http_response.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:cpap_mobile/device/connection/connection.helper.dart';

enum PatientEditType { Info, Status, History }

class PatientRepository {
  HTTPPatientService service = inject<HTTPPatientService>();

  Future<List<dynamic>> getPatients() async {
    HttpResponse response = HttpResponse();

    final hasConnection = await ConnectionHelper.hasConnection();

    if (hasConnection) {
      response = await this.service.getAllPatient();
    } else {
      response.message = "Device offline";
    }

    List<dynamic> results = [];
    if (response.statusCode == 200) {
      Map<String, dynamic> jsonResponse = response.data;
      if (jsonResponse.containsKey("data")) {
        final list = jsonResponse['data']['patients'] as List;
        if (list != null) {
          results = list;
        }
      }
    }
    return results;
  }

  Future<bool> addPatient(Patient patient) async {
    HttpResponse response = HttpResponse();

    final hasConnection = await ConnectionHelper.hasConnection();

    if (hasConnection) {
      response = await this.service.addPatient(patient);
    } else {
      response.message = "Device offline";
    }
    return response.statusCode == 200;
  }

  Future<bool> deletePatient(String patientId) async {
    HttpResponse response = HttpResponse();

    final hasConnection = await ConnectionHelper.hasConnection();

    if (hasConnection) {
      response = await this.service.deletePatient(patientId);
    } else {
      response.message = "Device offline";
    }
    return response.statusCode == 200;
  }

  Future<bool> editPatient(patient, PatientEditType infoType) async {
    HttpResponse response = HttpResponse();

    final hasConnection = await ConnectionHelper.hasConnection();

    if (hasConnection) {
      switch (infoType) {
        case PatientEditType.Info:
          response = await this.service.editPatientInfo(patient);
          break;
        case PatientEditType.Status:
          response = await this.service.editPatientStatus(patient);
          break;
        case PatientEditType.History:
          response = await this.service.editPatientHistory(patient);
          break;
      }
    } else {
      response.message = "Device offline";
    }
    return response.statusCode == 200;
  }
}
