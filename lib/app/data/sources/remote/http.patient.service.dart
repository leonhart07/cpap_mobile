import 'package:cpap_mobile/app/data/sources/cache/storage.helper.dart';
import 'package:cpap_mobile/app/data/sources/remote/common/unauthen_handler.dart';
import 'package:cpap_mobile/app/data/sources/remote/common/http_response.dart';
import 'package:cpap_mobile/app/data/structure/common.dart';
import 'package:cpap_mobile/core/di/http_client.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:cpap_mobile/core/values/keys.dart';
import 'package:cpap_mobile/app/data/sources/remote/base/server_endpoint.dart'
    as Endpoints;

class HTTPPatientService {
  HttpClient client = inject<HttpClient>();

  Future<HttpResponse> getAllPatient() async {
    HttpResponse response = HttpResponse();

    final String path = Endpoints.patientEndpoint.getUrl;
    final res = await client.get(path);

    int errorCode = res.data[statusCodeKey];
    invalidExpiredTokenHandler(errorCode);
    response.statusCode = errorCode;
    response.data = res.data;
    response.message = res.statusMessage;

    return response;
  }

  Future<HttpResponse> addPatient(Patient patient) async {
    HttpResponse response = HttpResponse();
    final token = await StorageHelper.get(StorageKeys.token);

    final String path = Endpoints.patientEndpoint.postUrl;
    final body = {
      "address_str": patient.address,
      "authen_token": token,
      "birthday": patient.birthday,
      "doctor_id": patient.doctorId,
      "email": patient.email,
      "fullname": patient.fullname,
      "phone_number": patient.phone
    };

    final res = await client.post(path, body: body);

    int errorCode = res.data[statusCodeKey];
    invalidExpiredTokenHandler(errorCode);
    response.statusCode = errorCode;
    response.data = res.data;
    response.message = res.statusMessage;

    return response;
  }

  Future<HttpResponse> deletePatient(String patientId) async {
    HttpResponse response = HttpResponse();

    final String path = Endpoints.patientEndpoint.deleteUrl(patientId);
    final token = await StorageHelper.get(StorageKeys.token);

    final payload = {'authen_token': token};
    final res = await client.delete(path, body: payload);

    int errorCode = res.data[statusCodeKey];
    invalidExpiredTokenHandler(errorCode);
    response.statusCode = errorCode;
    response.data = res.data;
    response.message = res.statusMessage;

    return response;
  }

  Future<HttpResponse> editPatientHistory(dynamic patient) async {
    HttpResponse response = HttpResponse();
    final token = await StorageHelper.get(StorageKeys.token);

    final String path = Endpoints.patientEndpoint.historyUrl(patient['id']);
    final body = {'authen_token': token, 'history': patient['history']};

    final res = await client.put(path, body: body);

    int errorCode = res.data[statusCodeKey];
    invalidExpiredTokenHandler(errorCode);
    response.statusCode = errorCode;
    response.data = res.data;
    response.message = res.statusMessage;

    return response;
  }

  Future<HttpResponse> editPatientStatus(dynamic patient) async {
    HttpResponse response = HttpResponse();
    final token = await StorageHelper.get(StorageKeys.token);

    final String path = Endpoints.patientEndpoint.statusUrl(patient['id']);
    final body = {'authen_token': token, 'status': patient['status']};

    final res = await client.put(path, body: body);

    int errorCode = res.data[statusCodeKey];
    invalidExpiredTokenHandler(errorCode);
    response.statusCode = errorCode;
    response.data = res.data;
    response.message = res.statusMessage;

    return response;
  }

  Future<HttpResponse> editPatientInfo(dynamic patient) async {
    HttpResponse response = HttpResponse();
    final token = await StorageHelper.get(StorageKeys.token);

    final String path = Endpoints.patientEndpoint.putUrl(patient['id']);
    final body = {
      'authen_token': token,
      'birthday': patient['birthday'],
      'fullname': patient['fullname'],
      'doctor_id': patient['doctor_id'],
      'phone_number': patient['phone_number'],
      'address_str': patient['address_str'],
      if (patient['email'].length > 0) 'email': patient['email'],
    };

    final res = await client.put(path, body: body);

    int errorCode = res.data[statusCodeKey];
    invalidExpiredTokenHandler(errorCode);
    response.statusCode = errorCode;
    response.data = res.data;
    response.message = res.statusMessage;

    return response;
  }
}
