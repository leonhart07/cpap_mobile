class AccountEndpointV1 {
  final postUrl = '/api/v1/account';
  final getUrl = '/api/v1/account';
  final pushNotificationSubUrl = '/api/v1/account/push_notification/sub';
  final pushNotificationMessageUrl =
      '/api/v1/account/push_notification/message';

  String putUrl(String accId) {
    return '/api/v1/account/' + accId;
  }

  String deleteUrl(String accId) {
    return '/api/v1/account/' + accId;
  }
}

class EquipmentEndpointV1 {
  final postUrl = '/api/v1/equipment';
  final getUrl = '/api/v1/equipment';
  String putUrl(String equipmentId) {
    return '/api/v1/equipment/' + equipmentId;
  }

  String deleteUrl(String equipmentId) {
    return '/api/v1/equipment/' + equipmentId;
  }

  String bookingUrl(String equipmentId) {
    return '/api/v1/equipment/' + equipmentId + '/booking';
  }
}

class PatientEndpointV1 {
  final postUrl = '/api/v1/patient';
  final getUrl = '/api/v1/patient';
  String putUrl(String patientId) {
    return '/api/v1/patient/' + patientId;
  }

  String deleteUrl(String patientId) {
    return '/api/v1/patient/' + patientId;
  }

  String statusUrl(String patientId) {
    return '/api/v1/patient/' + patientId + '/status';
  }

  String historyUrl(String patientId) {
    return '/api/v1/patient/' + patientId + '/history';
  }
}

class UserEndpointV1 {
  final changePasswordUrl = '/api/v1/user/password';
  final allAccountRoleUrl = '/api/v1/generic/user_role';
  final forgotPasswordUrl = '/api/v1/user/password';
  final loginUrl = '/api/v1/login';
  final authenTokenCheckUrl = '/api/v1/authen_token/check';
}

final userEndpoint = UserEndpointV1();
final accountEndpoint = AccountEndpointV1();
final patientEndpoint = PatientEndpointV1();
final equipmentEndpoint = EquipmentEndpointV1();
