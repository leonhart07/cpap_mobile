import 'package:cpap_mobile/app/data/sources/cache/storage.helper.dart';
import 'package:cpap_mobile/app/data/sources/remote/common/unauthen_handler.dart';
import 'package:cpap_mobile/app/data/sources/remote/common/http_response.dart';
import 'package:cpap_mobile/app/data/structure/common.dart';
import 'package:cpap_mobile/core/di/http_client.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:cpap_mobile/core/values/keys.dart';
import 'package:cpap_mobile/app/data/sources/remote/base/server_endpoint.dart'
    as Endpoints;

class HTTPAccountService {
  HttpClient client = inject<HttpClient>();

  // ==========================================================
  //                   ACCOUNT
  // ==========================================================
  Future<HttpResponse> getAllAccounts() async {
    HttpResponse response = HttpResponse();

    final String path = Endpoints.accountEndpoint.getUrl;
    final res = await client.get(path);

    int errorCode = res.data[statusCodeKey];
    invalidExpiredTokenHandler(errorCode);
    response.statusCode = errorCode;
    response.data = res.data;
    response.message = res.statusMessage;

    return response;
  }

  Future<HttpResponse> newAccount(Account account) async {
    HttpResponse response = HttpResponse();
    final token = await StorageHelper.get(StorageKeys.token);

    final String path = Endpoints.accountEndpoint.postUrl;
    final body = {
      'address_str': account.address,
      'authen_token': token,
      'birthday': account.birthday,
      'clinic': account.clinic,
      'email': account.email,
      'fullname': account.name,
      'hospital_faculty': null,
      'hospital_name': account.hospital,
      'password': account.password,
      'phone_number': account.phone,
      'role': account.role
    };

    final res = await client.post(path, body: body);

    int errorCode = res.data[statusCodeKey];
    invalidExpiredTokenHandler(errorCode);
    response.statusCode = errorCode;
    response.data = res.data;
    response.message = res.statusMessage;

    return response;
  }

  Future<HttpResponse> deleteAccount(String accId) async {
    HttpResponse response = HttpResponse();

    final String path = Endpoints.accountEndpoint.deleteUrl(accId);
    final token = await StorageHelper.get(StorageKeys.token);

    final payload = {'authen_token': token};
    final res = await client.delete(path, body: payload);

    int errorCode = res.data[statusCodeKey];
    invalidExpiredTokenHandler(errorCode);
    response.statusCode = errorCode;
    response.data = res.data;
    response.message = res.statusMessage;

    return response;
  }

  Future<HttpResponse> editAccount(Account account) async {
    HttpResponse response = HttpResponse();
    final token = await StorageHelper.get(StorageKeys.token);

    final String path = Endpoints.accountEndpoint.putUrl(account.id);
    final body = {
      'authen_token': token,
      'email': account.email,
      'fullname': account.name,
      'hospital_name': account.hospital,
      'phone_number': account.phone,
      'address_str': account.address,
      'birthday': account.birthday,
      'clinic': account.clinic
    };

    final res = await client.put(path, body: body);

    int errorCode = res.data[statusCodeKey];
    invalidExpiredTokenHandler(errorCode);
    response.statusCode = errorCode;
    response.data = res.data;
    response.message = res.statusMessage;

    return response;
  }
}
