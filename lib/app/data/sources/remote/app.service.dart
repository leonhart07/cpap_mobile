import 'package:cpap_mobile/app/data/push.notification.repository.dart';
import 'package:cpap_mobile/app/data/sources/cache/storage.helper.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class AppService {
  static PushNotificationRepository repository =
      inject<PushNotificationRepository>();

  static Future<void> clearForLogout() async {
    String fcmToken =
        await StorageHelper.get(StorageKeys.firebaseMessagingToken);
    if (fcmToken.isNotEmpty) {
      FirebaseMessaging.instance.deleteToken();
      await repository.unsub(fcmToken);
    }
    clearStorage();
  }

  static void clearStorage() {
    StorageHelper.set(StorageKeys.token, "");
    StorageHelper.set(StorageKeys.firebaseMessagingToken, "");
    StorageHelper.set(StorageKeys.login, "");
    StorageHelper.set(StorageKeys.password, "");
    StorageHelper.set(StorageKeys.role, '');
  }
}
