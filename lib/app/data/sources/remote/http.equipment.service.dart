import 'package:cpap_mobile/app/data/sources/cache/storage.helper.dart';
import 'package:cpap_mobile/app/data/sources/remote/common/unauthen_handler.dart';
import 'package:cpap_mobile/app/data/sources/remote/common/http_response.dart';
import 'package:cpap_mobile/core/di/http_client.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:cpap_mobile/core/values/keys.dart';
import 'package:cpap_mobile/app/data/sources/remote/base/server_endpoint.dart'
    as Endpoints;

class HTTPEquipmentService {
  HttpClient client = inject<HttpClient>();

  // ==========================================================
  //                   EQUIPMENT
  // ==========================================================
  Future<HttpResponse> getAllEquipments() async {
    HttpResponse response = HttpResponse();

    final String path = Endpoints.equipmentEndpoint.getUrl;
    final res = await client.get(path);

    int errorCode = res.data[statusCodeKey];
    invalidExpiredTokenHandler(errorCode);
    response.statusCode = errorCode;
    response.data = res.data;
    response.message = res.statusMessage;

    return response;
  }

  Future<HttpResponse> deleteEquipment(String equipmentId) async {
    HttpResponse response = HttpResponse();

    final String path = Endpoints.equipmentEndpoint.deleteUrl(equipmentId);
    final token = await StorageHelper.get(StorageKeys.token);
    final payload = {'authen_token': token};
    final res = await client.delete(path, body: payload);

    int errorCode = res.data[statusCodeKey];
    invalidExpiredTokenHandler(errorCode);
    response.statusCode = errorCode;
    response.data = res.data;
    response.message = res.statusMessage;

    return response;
  }

  Future<HttpResponse> newEquipment(String name) async {
    HttpResponse response = HttpResponse();

    final String path = Endpoints.equipmentEndpoint.postUrl;
    final token = await StorageHelper.get(StorageKeys.token);
    final body = {'authen_token': token, 'name': name};
    final res = await client.post(path, body: body);

    int statusCode = res.data[statusCodeKey];
    invalidExpiredTokenHandler(statusCode);
    response.statusCode = statusCode;
    response.data = res.data;

    return response;
  }

  Future<HttpResponse> editEquipmentInfo(equipment) async {
    HttpResponse response = HttpResponse();
    final token = await StorageHelper.get(StorageKeys.token);
    final String equipmentID = equipment['id'] ?? '';
    final String equipmentName = equipment['name'] ?? '';

    if (equipmentID.isEmpty || equipmentName.isEmpty) {
      response.statusCode = 404;
      response.data = '';
      response.message = 'error';
      return response;
    }
    String path = Endpoints.equipmentEndpoint.putUrl(equipmentID);
    final body = {'authen_token': token, 'name': equipmentName};
    final res = await client.put(path, body: body);

    int errorCode = res.data[statusCodeKey];
    invalidExpiredTokenHandler(errorCode);
    response.statusCode = errorCode;
    response.data = res.data;
    response.message = res.statusMessage;

    return response;
  }

  Future<HttpResponse> editEquipmentBooking(dynamic equipment) async {
    HttpResponse response = HttpResponse();
    final token = await StorageHelper.get(StorageKeys.token);
    final String equipmentID = equipment['id'] ?? '';

    if (equipmentID.isEmpty) {
      response.statusCode = 404;
      response.data = '';
      response.message = 'error';
      return response;
    }
    String path = Endpoints.equipmentEndpoint.bookingUrl(equipmentID);
    final body = {'authen_token': token, 'booking': equipment['booking']};

    final res = await client.put(path, body: body);

    int errorCode = res.data[statusCodeKey];
    invalidExpiredTokenHandler(errorCode);
    response.statusCode = errorCode;
    response.data = res.data;
    response.message = res.statusMessage;

    return response;
  }
}
