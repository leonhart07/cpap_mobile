import 'dart:convert';
import 'package:cpap_mobile/app/data/sources/cache/storage.helper.dart';
import 'package:cpap_mobile/app/data/sources/remote/common/unauthen_handler.dart';
import 'package:cpap_mobile/app/data/sources/remote/common/http_response.dart';
import 'package:cpap_mobile/app/data/structure/common.dart';
import 'package:cpap_mobile/core/di/http_client.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:cpap_mobile/core/values/keys.dart';
import 'package:cpap_mobile/app/data/sources/remote/base/server_endpoint.dart'
    as Endpoints;

class HTTPNotificationService {
  HttpClient client = inject<HttpClient>();

  // ==========================================================
  //                   SUBSCRIBE
  // ==========================================================

  Future<bool> sub(String fcmToken) async {
    HttpResponse response = HttpResponse();
    final token = await StorageHelper.get(StorageKeys.token);
    final String path = Endpoints.accountEndpoint.pushNotificationSubUrl;
    final deviceId = await StorageHelper.get(StorageKeys.deviceId);

    final payload = {
      'authen_token': token,
      'device_id': deviceId,
      'device_token': fcmToken
    };

    final body = jsonEncode(payload);
    final res = await client.post(path, body: body);

    int errorCode = res.data[statusCodeKey];
    invalidExpiredTokenHandler(errorCode);
    response.statusCode = errorCode;
    response.data = res.data;
    response.message = res.statusMessage;
    return response.statusCode == 200;
  }

  Future<bool> unsub(String fcmToken) async {
    HttpResponse response = HttpResponse();
    final String path = Endpoints.accountEndpoint.pushNotificationSubUrl;

    final token = await StorageHelper.get(StorageKeys.token);
    final deviceId = await StorageHelper.get(StorageKeys.deviceId);

    final payload = {
      'authen_token': token,
      'device_id': deviceId,
      'device_token': fcmToken
    };

    final body = jsonEncode(payload);
    final res = await client.delete(path, body: body);

    int statusCode = res.data[statusCodeKey];
    invalidExpiredTokenHandler(statusCode);
    response.statusCode = statusCode;
    response.data = res.data;
    response.message = res.statusMessage;

    return response.statusCode == 200;
  }

  // ==========================================================
  //                   MESSAGES
  // ==========================================================

  Future<HttpResponse> addNotification(NotificationPost notif) async {
    HttpResponse response = HttpResponse();
    final token = await StorageHelper.get(StorageKeys.token);

    String path = Endpoints.accountEndpoint.pushNotificationMessageUrl;
    final body = {
      'authen_token': token,
      'receiver': notif.accountID,
      'image': notif.image,
      'body': notif.body,
      'title': notif.title
    };

    final res = await client.post(path, body: body);

    int errorCode = res.data[statusCodeKey];
    invalidExpiredTokenHandler(errorCode);
    response.statusCode = errorCode;
    response.data = res.data;
    response.message = res.statusMessage;

    return response;
  }

  Future<HttpResponse> getNotifications(int pageNo) async {
    HttpResponse response = HttpResponse();

    final String path = Endpoints.accountEndpoint.pushNotificationMessageUrl;
    final Map<String, dynamic> payload = {'page_no': pageNo, "page_length": 20};
    final res = await client.getWithParams(path, payload);

    int errorCode = res.data[statusCodeKey];
    invalidExpiredTokenHandler(errorCode);
    response.statusCode = errorCode;
    response.data = res.data;
    response.message = res.statusMessage;

    return response;
  }
}
