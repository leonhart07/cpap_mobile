import 'dart:convert';
import 'package:cpap_mobile/app/data/sources/cache/storage.helper.dart';
import 'package:cpap_mobile/app/data/sources/remote/app.service.dart';
import 'package:cpap_mobile/app/data/sources/remote/common/unauthen_handler.dart';
import 'package:cpap_mobile/app/data/sources/remote/common/http_response.dart';
import 'package:cpap_mobile/core/di/http_client.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:cpap_mobile/core/values/keys.dart';
import 'package:cpap_mobile/app/data/sources/remote/base/server_endpoint.dart'
    as Endpoints;

class HTTPUserService {
  HttpClient client = inject<HttpClient>();

  // ==========================================================
  //                   USER LOGIN
  // ==========================================================

  Future<HttpResponse> login(String login, String password) async {
    HttpResponse response = HttpResponse();

    final String path = Endpoints.userEndpoint.loginUrl;
    final deviceId = await StorageHelper.get(StorageKeys.deviceId);
    final payload = {
      'email': login,
      'password': password,
      'device_id': deviceId,
    };
    final body = jsonEncode(payload);
    final res = await client.post(path, body: body);

    int statusCode = res.data[statusCodeKey];
    invalidExpiredTokenHandler(statusCode);
    if (statusCode == 200) {
      String token = res.data['data']["authen_token"];
      String role = res.data['data']['role'];
      StorageHelper.set(StorageKeys.token, token);
      StorageHelper.set(StorageKeys.login, login);
      StorageHelper.set(StorageKeys.password, password);
      StorageHelper.set(StorageKeys.role, role);
      response.statusCode = statusCode;
      response.data = res.data;
      response.message = res.statusMessage;
    } else {
      response.statusCode = statusCode;
      response.message = 'unknow error';
      Map<String, dynamic> data = res.data;
      if (data.containsKey('message')) {
        response.message = data['message'];
      }
      AppService.clearStorage();
    }

    return response;
  }

  Future<HttpResponse> loginWithToken(String token) async {
    HttpResponse response = HttpResponse();
    final String path = Endpoints.userEndpoint.authenTokenCheckUrl;
    final payload = {'authen_token': token};
    final body = jsonEncode(payload);
    final res = await client.post(path, body: body);
    int statusCode = res.data[statusCodeKey];
    invalidExpiredTokenHandler(statusCode);
    response.statusCode = statusCode;
    response.data = res.data;
    response.message = res.statusMessage;

    return response;
  }

  // ==========================================================
  //                   USER PASSWORD
  // ==========================================================

  Future<HttpResponse> forgotPassword(String email) async {
    HttpResponse response = HttpResponse();

    final String path = Endpoints.userEndpoint.forgotPasswordUrl;
    final payload = {'email': email};
    final body = jsonEncode(payload);
    final res = await client.post(path, body: body);
    int errorCode = res.data[statusCodeKey];
    invalidExpiredTokenHandler(errorCode);
    response.statusCode = errorCode;
    response.data = res.data;
    response.message = res.statusMessage;
    return response;
  }

  Future<HttpResponse> changePassword(
      String oldPassword, String newPassword) async {
    HttpResponse response = HttpResponse();
    final token = await StorageHelper.get(StorageKeys.token);
    final String path = Endpoints.userEndpoint.changePasswordUrl;

    final payload = {
      'authen_token': token,
      'old_password': oldPassword,
      'new_password': newPassword
    };

    final body = jsonEncode(payload);
    final res = await client.put(path, body: body);
    int errorCode = res.data[statusCodeKey];
    invalidExpiredTokenHandler(errorCode);
    response.statusCode = errorCode;
    response.data = res.data;
    return response;
  }

  // ==========================================================
  //                   USER UTILITIES
  // ==========================================================
  Future<HttpResponse> getRoleResponse() async {
    HttpResponse response = HttpResponse();

    final String path = Endpoints.userEndpoint.allAccountRoleUrl;
    final res = await client.get(path);
    int errorCode = res.data[statusCodeKey];
    invalidExpiredTokenHandler(errorCode);
    response.statusCode = errorCode;
    response.data = res.data;
    response.message = res.statusMessage;

    return response;
  }
}
