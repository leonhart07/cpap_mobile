import 'package:cpap_mobile/app/data/sources/cache/storage.helper.dart';
import 'package:get/get.dart';
import 'package:cpap_mobile/app/ui/modules/unauthenticated/login/login.page.dart';

void invalidExpiredTokenHandler(int statusCode) async {
  List<int> tokenFailCode = [421, 422];
  if (tokenFailCode.contains(statusCode)) {
    String email = await StorageHelper.get(StorageKeys.login);
    String password = await StorageHelper.get(StorageKeys.password);
    Get.to(() => LoginPage(email: email, password: password));
  }
}
