import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';

typedef TextPickerCallBack = void Function(String value);

class TextPicker extends StatefulWidget {
  final TextPickerCallBack callback;
  TextPicker(
      {this.initValue = '',
      this.hintText = 'Input',
      this.iconPath = 'assets/svg/ic_pencil.svg',
      this.iconHeight = 12,
      this.height = 48,
      this.width = 48,
      this.radius = 24,
      this.isPassword = false,
      this.passwordVisible = true,
      this.numberOnly = false,
      this.fontSize = 14,
      this.textLength = 64,
      this.textLine = 1,
      this.callback});
  final String initValue;
  final String hintText;
  final String iconPath;
  final double iconHeight;
  final double width;
  final double height;
  final double radius;
  final bool isPassword;
  final bool passwordVisible;
  final bool numberOnly;
  final double fontSize;
  final int textLength;
  final int textLine;
  @override
  _TextPickerState createState() => _TextPickerState();
}

class _TextPickerState extends State<TextPicker> {
  TextEditingController controller = new TextEditingController();
  bool showPassword = false;

  @override
  void initState() {
    String initValue = widget.initValue;
    showPassword = !widget.isPassword;
    if (widget.initValue == null) initValue = '';
    controller.value = TextEditingValue(
      text: initValue,
      selection: TextSelection.collapsed(offset: initValue.length),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onHorizontalDragEnd: (dragEndDetails) {
          if (dragEndDetails.primaryVelocity < 0) {
            // swipe to left
            controller.clear();
            if (widget.callback != null) widget.callback('');
          } else if (dragEndDetails.primaryVelocity > 0) {
            // swipe to right
          }
        },
        child: Container(
          width: widget.width,
          height: widget.height,
          child: TextField(
            inputFormatters: [
              LengthLimitingTextInputFormatter(widget.textLength),
            ],
            textAlignVertical: TextAlignVertical.center,
            style: TextStyle(color: Colors.black, fontSize: widget.fontSize),
            maxLines: widget.textLine,
            controller: controller,
            keyboardType: widget.isPassword
                ? TextInputType.visiblePassword
                : widget.numberOnly
                    ? TextInputType.number
                    : TextInputType.emailAddress,
            obscureText: !showPassword,
            onChanged: (value) {
              widget.callback(value);
            },
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(8.0),
              hintText: widget.hintText,
              prefixIcon: widget.iconPath.isNotEmpty
                  ? Padding(
                      padding: const EdgeInsets.only(
                          right: 16.0, left: 8.0, top: 8.0, bottom: 8.0),
                      child: SizedBox(
                          child: SvgPicture.asset(widget.iconPath,
                              semanticsLabel: '', width: widget.iconHeight)))
                  : null,
              suffixIcon: widget.isPassword
                  ? IconButton(
                      icon: Icon(
                        showPassword ? Icons.visibility : Icons.visibility_off,
                        color: Theme.of(context).primaryColorDark,
                      ),
                      onPressed: () {
                        FocusScope.of(context).unfocus();
                        if (widget.passwordVisible)
                          showPassword = !showPassword;
                      },
                    )
                  : null,
              fillColor: Colors.white,
              filled: true,
              border: widget.radius > 0
                  ? OutlineInputBorder(
                      borderRadius: BorderRadius.circular(widget.radius / 2),
                    )
                  : InputBorder.none,
            ),
          ),
        ));
  }
}
