import 'package:cpap_mobile/app/data/sources/remote/app.service.dart';
import 'package:cpap_mobile/app/ui/components/date_picker.dart';
import 'package:cpap_mobile/app/ui/components/list_picker.dart';
import 'package:cpap_mobile/app/ui/components/multiSelection.dart';
import 'package:cpap_mobile/app/ui/components/text_picker.dart';
import 'package:cpap_mobile/app/ui/modules/unauthenticated/login/login.page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'dart:math' as math;

typedef AddButtonCallback = Future<void> Function(BuildContext context);
typedef MultiSelectionAddCallback = void Function(
    BuildContext context, List<dynamic> values);
typedef MultiSelectionRemoveCallback = void Function(
    BuildContext context, dynamic value);

TextStyle pageTitleTextStyle({double fontSize: 14.0, Color color}) {
  return TextStyle(
    color: (color == null) ? Color.fromRGBO(82, 95, 127, 1.0) : color,
    fontSize: fontSize,
    fontWeight: FontWeight.bold,
  );
}

Widget pageTitleWidget(BuildContext context, String value,
    {double height: 44,
    double fontSize: 14.0,
    Color color,
    List<Widget> additionWiget}) {
  List<Widget> extraWidget = [];
  if (additionWiget != null) extraWidget = additionWiget;
  return Column(
    mainAxisAlignment: MainAxisAlignment.spaceAround,
    mainAxisSize: MainAxisSize.min,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Container(
          width: (MediaQuery.maybeOf(context).size.width / 2),
          height: height,
          child: Text(
            value,
            style: pageTitleTextStyle(fontSize: fontSize, color: color),
            textAlign: TextAlign.center,
          )),
      for (var i = 0; i < extraWidget?.length; i++) extraWidget[i]
    ],
  );
}

Widget logoutButton(BuildContext context) {
  return IconButton(
    onPressed: () async {
      await AppService.clearForLogout();
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => LoginPage(),
        ),
      );
    },
    icon: logoutIcon(),
  );
}

Widget logoutIcon() {
  return Transform.rotate(
      angle: -math.pi,
      child: Transform(
          alignment: Alignment.center,
          transform: Matrix4.rotationY(math.pi),
          child: ClipRect(
            child: Container(
                margin: const EdgeInsets.only(
                  bottom: 0.0,
                  left: 0.0,
                ),
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      offset: Offset(0, 0),
                      blurRadius: 5.0,
                    ),
                  ],
                ),
                child: SvgPicture.asset('assets/svg/ic_exit3.svg')),
          )));
}

Widget addingButton(BuildContext context, AddButtonCallback callback) {
  return Padding(
      padding: EdgeInsets.only(right: 8.0),
      child: FloatingActionButton(
          elevation: 5.0,
          child: Icon(Icons.add),
          backgroundColor: Colors.blue,
          mini: true,
          onPressed: () {
            callback(context);
          }));
}

Widget createTextWidget(
    BuildContext context, String hint, String value, TextPickerCallBack setVal,
    {String iconName = 'ic_pencil.svg',
    bool isPassword: false,
    bool isNumber: false,
    bool passwordVisible: true,
    double width: 0,
    double height: 50,
    double radius: 24,
    int textLength: 64,
    int textLine: 1}) {
  double widgetWdidth = MediaQuery.maybeOf(context).size.width - 68;
  if (width > 0) widgetWdidth = width;
  return TextPicker(
      width: widgetWdidth,
      height: height,
      hintText: hint,
      iconPath: iconName.isNotEmpty ? 'assets/svg/' + iconName : '',
      isPassword: isPassword,
      passwordVisible: passwordVisible,
      numberOnly: isNumber,
      initValue: value,
      radius: radius,
      callback: setVal,
      textLength: textLength,
      textLine: textLine);
}

Widget createListWidget(BuildContext context, String hint,
    Map<String, Object> items, ListPickerCallBack setVal,
    {double width: 0,
    double height: 50,
    double radius: 24,
    TextStyle textStyle,
    textAlign: TextAlign.right,
    String initValue}) {
  double widgetWdidth = MediaQuery.maybeOf(context).size.width - 68;
  if (width > 0) widgetWdidth = width;
  return ListPicker(
      width: widgetWdidth,
      height: height,
      hintText: hint,
      selectionItems: items,
      radius: radius,
      textStyle: textStyle,
      textAlign: textAlign,
      callback: setVal,
      initValue: initValue);
}

Widget createDateTimeWidget(
    BuildContext context, String hint, String value, TextPickerCallBack setVal,
    {String iconName = 'ic_calendar.svg',
    String helpText = 'Ngày Sinh',
    double width: 0,
    double height: 50,
    double radius: 24,
    int passYear: 100,
    int futureYear: 25}) {
  double widgetWdidth = MediaQuery.maybeOf(context).size.width - 68;
  if (width > 0) widgetWdidth = width;

  void valueCallback(DateTime value) {
    if (value == null)
      setVal('');
    else
      setVal(DateFormat('dd/MM/yyyy').format(value));
  }

  return DatePicker(
      width: widgetWdidth,
      height: height,
      hintText: hint,
      helpText: helpText,
      confirmText: 'Chọn',
      cancelText: 'Hủy',
      iconPath: iconName.isNotEmpty ? 'assets/svg/' + iconName : '',
      passYear: passYear,
      futureYear: futureYear,
      initValue: value,
      radius: radius,
      callback: valueCallback);
}

Widget createMultiSelectWidget(
  BuildContext context,
  Map<String, Object> allItems,
  MultiSelectionAddCallback addCallback,
  MultiSelectionRemoveCallback removeCallback, {
  String hint: 'Select',
  String cancelText: 'Cancel',
  String confirmText: 'OK',
  double width: 0,
}) {
  double widgetWdidth = MediaQuery.maybeOf(context).size.width - 68;
  if (width > 0) widgetWdidth = width;
  return MultiSelectionWidget(
    width: widgetWdidth,
    allItems: allItems,
    addCallback: addCallback,
    removeCallback: removeCallback,
    hint: hint,
    cancelText: cancelText,
    confirmText: confirmText,
  );
}
