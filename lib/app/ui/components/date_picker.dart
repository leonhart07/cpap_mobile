import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';

typedef DatePickerCallBack = void Function(DateTime date);

class DatePicker extends StatefulWidget {
  final DatePickerCallBack callback;
  DatePicker(
      {this.helpText = 'Booking date',
      this.cancelText = 'Cancel',
      this.confirmText = 'Book',
      this.iconPath = 'assets/svg/ic_calendar.svg',
      this.height = 48,
      this.width = 48,
      this.hintText = 'Ngày',
      this.radius = 12,
      this.initValue = '',
      this.passYear = 100,
      this.futureYear = 25,
      this.format = 'dd/MM/yyyy',
      this.fontSize = 14,
      this.callback});

  final String helpText;
  final String cancelText;
  final String confirmText;
  final String iconPath;
  final double width;
  final double height;
  final String hintText;
  final double radius;
  final int passYear;
  final int futureYear;
  final String initValue;
  final String format;
  final double fontSize;
  @override
  _DatePickerState createState() => _DatePickerState();
}

class _DatePickerState extends State<DatePicker> {
  DateTime currentDate = DateTime.now();
  DateTime fromDate = DateTime.now();
  DateTime lastDate = DateTime.now();
  TextEditingController controller = new TextEditingController();

  bool _decideWhichDayToEnable(DateTime day) {
    if (day.isAfter(fromDate.subtract(Duration(days: 1)))) {
      return true;
    }
    return false;
  }

  /// This builds material date picker in Android
  Future<Null> buildMaterialDatePicker(BuildContext context) async {
    // Adjut initialDate be on or after firstDate
    if (currentDate.isBefore(fromDate))
      fromDate = currentDate.subtract(Duration(days: 1));

    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: currentDate,
      firstDate: fromDate,
      lastDate: lastDate,
      selectableDayPredicate: _decideWhichDayToEnable,
      errorFormatText: 'dd/MM/yyyy',
      errorInvalidText: 'Date is invalid',
      helpText: widget.helpText,
      cancelText: widget.cancelText,
      confirmText: widget.confirmText,
      builder: (context, child) {
        return Theme(
          data: ThemeData.light(),
          child: child,
        );
      },
    );
    if (picked != null && picked != currentDate)
      setState(() {
        if (widget.callback != null) widget.callback(picked);
        controller.value =
            TextEditingValue(text: DateFormat(widget.format).format(picked));
        currentDate = picked;
      });
  }

  /// This builds cupertion date picker in iOS
  Future<Null> buildCupertinoDatePicker(BuildContext context) {
    return showModalBottomSheet(
        context: context,
        builder: (BuildContext builder) {
          return Container(
            height: MediaQuery.of(context).copyWith().size.height / 3,
            color: Colors.white,
            child: CupertinoDatePicker(
              mode: CupertinoDatePickerMode.date,
              onDateTimeChanged: (picked) {
                if (picked != null && picked != currentDate)
                  setState(() {
                    currentDate = picked;
                    if (widget.callback != null) widget.callback(picked);
                    controller.value =
                        TextEditingValue(text: picked.toString());
                  });
              },
              initialDateTime: currentDate,
              minimumYear: currentDate.year - widget.passYear,
              maximumYear: currentDate.year + widget.futureYear,
            ),
          );
        });
  }

  Future<Null> _selectDate(BuildContext context) async {
    final ThemeData theme = Theme.of(context);
    assert(theme.platform != null);
    switch (theme.platform) {
      case TargetPlatform.android:
      case TargetPlatform.fuchsia:
      case TargetPlatform.linux:
      case TargetPlatform.windows:
        return buildMaterialDatePicker(context);
      case TargetPlatform.iOS:
      case TargetPlatform.macOS:
        return buildCupertinoDatePicker(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    String initValue = widget.initValue ?? '';
    controller.value = TextEditingValue(text: initValue);
    try {
      currentDate = DateFormat(widget.format).parse(initValue);
    } catch (e) {
      currentDate = DateTime.now();
    }
    fromDate = DateTime(currentDate.year - widget.passYear,
        DateTime.now().month, DateTime.now().day);
    lastDate = DateTime(currentDate.year + widget.futureYear,
        DateTime.now().month, DateTime.now().day);

    return GestureDetector(
        onHorizontalDragEnd: (dragEndDetails) {
          if (dragEndDetails.primaryVelocity < 0) {
            // swipe to left
            controller.clear();
            if (widget.callback != null) widget.callback(null);
          } else if (dragEndDetails.primaryVelocity > 0) {
            // swipe to right
          }
        },
        onTap: () => _selectDate(context),
        child: Container(
            width: widget.width,
            height: widget.height,
            child: AbsorbPointer(
              child: TextField(
                textAlignVertical: TextAlignVertical.center,
                style:
                    TextStyle(color: Colors.black, fontSize: widget.fontSize),
                maxLines: 1,
                controller: controller,
                keyboardType: TextInputType.datetime,
                readOnly: true,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(8.0),
                  hintText: widget.hintText,
                  prefixIcon: widget.iconPath.isNotEmpty
                      ? Padding(
                          padding: const EdgeInsets.only(
                              right: 16.0, left: 8.0, top: 8.0, bottom: 8.0),
                          child: SizedBox(
                              child: SvgPicture.asset(widget.iconPath,
                                  semanticsLabel: '',
                                  width: widget.height / 4)))
                      : null,
                  fillColor: Colors.white,
                  filled: true,
                  border: widget.radius > 0
                      ? OutlineInputBorder(
                          borderRadius: BorderRadius.circular(widget.radius))
                      : InputBorder.none,
                ),
              ),
            )));
  }
}
