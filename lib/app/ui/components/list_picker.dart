import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';

typedef ListPickerCallBack = void Function(String value);

class ListPicker extends StatefulWidget {
  final ListPickerCallBack callback;
  ListPicker(
      {this.selectionItems,
      this.hintText = 'Input',
      this.iconPath = 'assets/svg/ic_dropdown.svg',
      this.height = 48,
      this.width = 48,
      this.radius = 24,
      this.textStyle,
      this.textAlign,
      this.initValue,
      this.callback});
  final String hintText;
  final String iconPath;
  final double width;
  final double height;
  final double radius;
  final TextStyle textStyle;
  final TextAlign textAlign;
  final Map<String, Object> selectionItems;
  final initValue;
  @override
  _ListPickerState createState() => _ListPickerState();
}

class _ListPickerState extends State<ListPicker> {
  var selectedValue;

  @override
  void initState() {
    if (widget.initValue != null) {
      if (widget.selectionItems.containsKey(widget.initValue))
        selectedValue = widget.initValue;
      else
        selectedValue ??= widget.selectionItems.keys.first;
    } else
      selectedValue ??= widget.selectionItems.keys.first;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: widget.width,
        height: widget.height,
        decoration: BoxDecoration(
            color: Colors.white,
            border:
                widget.radius > 0 ? Border.all(color: Colors.black) : Border(),
            borderRadius: BorderRadius.circular(widget.radius / 2)),
        child: DropdownButtonHideUnderline(
          child: DropdownButton(
            isExpanded: true,
            icon: Padding(
                padding: EdgeInsets.only(right: widget.radius > 0 ? 16 : 0),
                child: SvgPicture.asset(widget.iconPath,
                    semanticsLabel: '', width: widget.height / 2)),
            dropdownColor: Colors.white,
            elevation: 16,
            value: selectedValue,
            items: [
              for (String key in widget.selectionItems.keys)
                DropdownMenuItem(
                  child: Container(
                    width: widget.width,
                    height: widget.height,
                    child: Align(
                        alignment: Alignment.center,
                        child: Text(
                            widget.selectionItems[key].toString().trim(),
                            textAlign: widget.textAlign,
                            style: widget.textStyle,
                            overflow: TextOverflow.ellipsis)),
                  ),
                  value: key,
                )
            ],
            onChanged: (key) {
              this.selectedValue = key;
              widget?.callback(key);
            },
            hint: Padding(
              padding: EdgeInsets.only(left: 16),
              child: Text(
                widget.hintText,
                style: widget.textStyle,
                textAlign: widget.textAlign,
              ),
            ),
            style: TextStyle(color: Colors.black, decorationColor: Colors.red),
          ),
        ));
  }
}
