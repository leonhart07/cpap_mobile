import 'package:cpap_mobile/app/ui/components/expandable_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

typedef ElvCbEditAction = Future<void> Function(
    BuildContext context, dynamic item);
typedef ElvCbDeleteAction = void Function(dynamic item);
typedef ElvCbGetWidget = Widget Function(Map<String, dynamic> item);
typedef ElvCbGetListWidget = List<Widget> Function(Map<String, dynamic> item);
typedef ElvCbGetChildItemCount = int Function(Map<String, dynamic> item);
typedef ElvCbGetChildItemWidget = Widget Function(
    Map<String, dynamic> item, int index);
typedef ElvCbActionOnPres = void Function(BuildContext context, dynamic item);

void showConfirmDialog(BuildContext context, Widget title, Widget content,
    dynamic item, ElvCbDeleteAction confirmAction) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: title,
        content: content,
        actions: [
          TextButton(
            child: Text("No"),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          TextButton(
            child: Text("Yes"),
            onPressed: () {
              confirmAction(item);
              Navigator.pop(context);
            },
          ),
        ],
      );
    },
  );
}

class ExpandableListViewScrollable extends StatefulWidget {
  const ExpandableListViewScrollable(
      {Key key,
      @required this.itemsList,
      this.editable = true,
      this.editItem,
      this.deleteItem,
      this.childBackgroundColor = Colors.grey,
      this.itemBackgroundColor = Colors.white,
      this.itemExpandedBackgroundColor = Colors.blueAccent,
      this.expansionLength = 250.0,
      this.expandButtonImage,
      this.itemPrimaryWidget,
      this.itemSecondaryWidget,
      this.childItemCount,
      this.childItemWindow = 4,
      this.onPressCallback,
      this.onLongPressCallback,
      @required this.childItemBuilder})
      : assert(editItem != null),
        assert(deleteItem != null),
        assert(expandButtonImage != null),
        assert(itemPrimaryWidget != null),
        assert(itemSecondaryWidget != null),
        assert(childItemBuilder != null),
        super(key: key);

  final itemsList;
  final bool editable;
  final ElvCbEditAction editItem;
  final ElvCbDeleteAction deleteItem;
  final Color childBackgroundColor;
  final Color itemBackgroundColor;
  final Color itemExpandedBackgroundColor;
  final double expansionLength;
  final ElvCbGetWidget expandButtonImage;
  final ElvCbGetListWidget itemPrimaryWidget;
  final ElvCbGetWidget itemSecondaryWidget;
  final ElvCbGetChildItemCount childItemCount;
  final ElvCbGetChildItemWidget childItemBuilder;
  final int childItemWindow;
  final ElvCbActionOnPres onPressCallback;
  final ElvCbActionOnPres onLongPressCallback;

  @override
  _ExpandableListViewScrollableState createState() =>
      _ExpandableListViewScrollableState();
}

class _ExpandableListViewScrollableState
    extends State<ExpandableListViewScrollable> {
  @override
  Widget build(BuildContext context) {
    int itemCount = 0;
    try {
      itemCount = widget.itemsList?.length;
    } catch (e) {
      print(e);
      itemCount = 0;
    }
    var physicScroller = const AlwaysScrollableScrollPhysics();

    return Expanded(
      child: RawScrollbar(
          thickness: 5,
          thumbColor: Colors.blueAccent,
          radius: Radius.circular(20),
          timeToFade: Duration(milliseconds: 200),
          child: ListView.builder(
            controller: TrackingScrollController(),
            physics: physicScroller,
            itemCount: itemCount,
            itemBuilder: (context, index) {
              Widget elementItem = ExpandableListViewItem(
                  key: GlobalKey(),
                  item: widget.itemsList[index],
                  expandedContentBackgroundColor: widget.childBackgroundColor,
                  tileBackgroundColor: widget.itemBackgroundColor,
                  tileExpandedBackgroundColor:
                      widget.itemExpandedBackgroundColor,
                  expansionLength: widget.expansionLength,
                  buttonImage: widget.expandButtonImage,
                  delegatePrimaryWidget: widget.itemPrimaryWidget,
                  delegateSecondaryWidget: widget.itemSecondaryWidget,
                  expandItemLength: widget.childItemCount,
                  expandElvCbGetWidget: widget.childItemBuilder,
                  expandItemWindow: widget.childItemWindow,
                  scrollController: physicScroller,
                  onPressCallback: widget.onPressCallback,
                  onLongPressCallback: widget.onLongPressCallback);
              if (widget.editable)
                return new Slidable(
                    actionPane: SlidableDrawerActionPane(),
                    actionExtentRatio: 0.12,
                    secondaryActions: <Widget>[
                      SizedBox(
                        height: 44,
                        child: IconSlideAction(
                            caption: "Edit",
                            color: Colors.grey,
                            icon: Icons.edit,
                            onTap: () => widget.editItem(
                                context, widget.itemsList[index])),
                      ),
                      SizedBox(
                          height: 44,
                          child: IconSlideAction(
                              caption: "Delete",
                              color: Colors.red,
                              icon: Icons.delete,
                              onTap: () {
                                var content = widget
                                    .itemPrimaryWidget(widget.itemsList[index]);
                                showConfirmDialog(
                                    context,
                                    Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: content),
                                    Text('Sẽ bị xóa khỏi danh sách?',
                                        style: TextStyle(color: Colors.red)),
                                    widget.itemsList[index],
                                    widget.deleteItem);
                              })),
                    ],
                    child: elementItem);
              else
                return elementItem;
            },
          )),
    );
  }
}

class ExpandableListViewItem extends StatefulWidget {
  const ExpandableListViewItem(
      {Key key,
      @required this.item,
      this.expandedContentBackgroundColor = Colors.grey,
      this.tileBackgroundColor = Colors.white,
      this.tileExpandedBackgroundColor = Colors.blueAccent,
      this.expansionLength = 250.0,
      this.buttonImage,
      this.delegatePrimaryWidget,
      this.delegateSecondaryWidget,
      this.expandItemLength,
      this.expandElvCbGetWidget,
      this.expandItemWindow = 4,
      this.onPressCallback,
      this.onLongPressCallback,
      this.scrollController})
      : assert(buttonImage != null),
        assert(delegatePrimaryWidget != null),
        assert(delegateSecondaryWidget != null),
        assert(expandElvCbGetWidget != null),
        super(key: key);

  final item;
  final Color expandedContentBackgroundColor;
  final Color tileBackgroundColor;
  final Color tileExpandedBackgroundColor;
  final double expansionLength;
  final ElvCbGetWidget buttonImage;
  final ElvCbGetListWidget delegatePrimaryWidget;
  final ElvCbGetWidget delegateSecondaryWidget;
  final ElvCbGetChildItemCount expandItemLength;
  final ElvCbGetChildItemWidget expandElvCbGetWidget;
  final int expandItemWindow;
  final scrollController;
  final ElvCbActionOnPres onPressCallback;
  final ElvCbActionOnPres onLongPressCallback;

  @override
  _ExpandableListViewItemState createState() => _ExpandableListViewItemState();
}

class _ExpandableListViewItemState extends State<ExpandableListViewItem> {
  final double size = 22;
  bool expandFlag = false;
  Color expandedColor = Colors.white;
  Color tileColor = Colors.white;

  void ensureContentView(GlobalKey contentKey) {
    if (contentKey.currentContext != null) {
      Future.delayed(Duration(milliseconds: 200)).then((value) {
        Scrollable.ensureVisible(contentKey.currentContext,
            duration: Duration(milliseconds: 200));
      });
    }
  }

  String getValueFromDictionary(String key, Map<String, dynamic> dict) {
    if (dict.containsKey(key)) return dict[key];
    return '';
  }

  @override
  Widget build(BuildContext context) {
    final int expandedItemsCount = widget.expandItemLength(widget.item);
    final double expandedHeight =
        expandedItemsCount * 50 > widget.expansionLength
            ? widget.expansionLength
            : expandedItemsCount * 50 + 0.0;
    return new Container(
      margin: new EdgeInsets.symmetric(vertical: 1.0),
      child: new Column(
        children: <Widget>[
          new Container(
              color: tileColor,
              padding: new EdgeInsets.symmetric(horizontal: 5.0),
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    elevation: 0.0,
                    shadowColor: Colors.white.withOpacity(0.05),
                    primary: Colors.white.withOpacity(0.05), // background
                    onPrimary: Colors.white.withOpacity(0.05), // foreground
                  ),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(children: <Widget>[
                          widget.buttonImage(widget.item),
                          SizedBox(width: 16),
                          Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children:
                                  widget.delegatePrimaryWidget(widget.item))
                        ]),
                        widget.delegateSecondaryWidget(widget.item)
                      ]),
                  onPressed: () {
                    setState(() {
                      if (widget.onPressCallback != null)
                        widget.onPressCallback(context, widget.item);
                      else {
                        expandFlag = !expandFlag;
                        if (expandFlag) {
                          tileColor = widget.tileExpandedBackgroundColor;
                          expandedColor = widget.expandedContentBackgroundColor;
                        } else {
                          tileColor = widget.tileBackgroundColor;
                          expandedColor = widget.tileBackgroundColor;
                        }
                        ensureContentView(widget.key);
                      }
                    });
                  },
                  onLongPress: () {
                    if (widget.onLongPressCallback != null)
                      widget.onLongPressCallback(context, widget.item);
                  })),
          if (widget.onPressCallback == null && expandedItemsCount > 0)
            new ExpandableContainer(
                expandedHeight: expandedHeight,
                expanded: expandFlag,
                child: CupertinoScrollbar(
                    child: ListView.builder(
                        physics: widget.scrollController,
                        itemCount: expandedItemsCount,
                        itemExtent: expandedHeight / expandedItemsCount,
                        // (expandedItemsCount > widget.expandItemWindow)
                        //     ? expandedHeight / widget.expandItemWindow
                        //     : expandedHeight,
                        itemBuilder: (BuildContext context, int index) {
                          return ConstrainedBox(
                              constraints: BoxConstraints(
                                maxWidth:
                                    MediaQuery.maybeOf(context).size.width,
                                maxHeight: expandedHeight,
                              ),
                              child: widget.expandElvCbGetWidget(
                                  widget.item, index));
                        })))
        ],
      ),
    );
  }
}
