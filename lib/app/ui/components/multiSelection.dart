import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';

typedef MultiSelectionAddCallback = void Function(
    BuildContext context, List<dynamic> values);
typedef MultiSelectionRemoveCallback = void Function(
    BuildContext context, dynamic value);

class MultiSelectionWidget extends StatefulWidget {
  MultiSelectionWidget({
    Key key,
    this.allItems,
    this.addCallback,
    this.removeCallback,
    this.hint = 'Select',
    this.cancelText = 'Cancel',
    this.confirmText = 'OK',
    this.cancelButtonTextStyle,
    this.confirmButtonTextStyle,
    this.itemsTextStyle,
    this.selectedColor = Colors.blue,
    this.unselectedColor = Colors.grey,
    this.width,
  }) : super(key: key);

  final Map<String, Object> allItems;
  final MultiSelectionAddCallback addCallback;
  final MultiSelectionRemoveCallback removeCallback;
  final String hint;
  final String cancelText;
  final String confirmText;
  final TextStyle confirmButtonTextStyle;
  final TextStyle cancelButtonTextStyle;
  final TextStyle itemsTextStyle;
  final Color selectedColor;
  final Color unselectedColor;
  final double width;

  @override
  _MultiSelectionWidgetState createState() => _MultiSelectionWidgetState();
}

class _MultiSelectionWidgetState extends State<MultiSelectionWidget> {
  List<MultiSelectItem> allItemList = [];
  List selectedItems = [];
  TextStyle confirmButtonTextStyle;
  TextStyle cancelButtonTextStyle;
  TextStyle itemsTextStyle;
  @override
  void initState() {
    allItemList = widget.allItems.entries
        .map((entry) => MultiSelectItem(entry.key, entry.value))
        .toList();

    confirmButtonTextStyle = (widget.confirmButtonTextStyle != null)
        ? widget.confirmButtonTextStyle
        : TextStyle(
            color: Colors.blue,
            fontSize: 16,
          );

    cancelButtonTextStyle = (widget.cancelButtonTextStyle != null)
        ? widget.cancelButtonTextStyle
        : TextStyle(
            color: Colors.red,
            fontSize: 16,
          );

    itemsTextStyle = (widget.itemsTextStyle != null)
        ? widget.itemsTextStyle
        : TextStyle(
            color: Colors.blue[800],
            fontSize: 12,
          );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: widget.width,
        child: MultiSelectBottomSheetField(
          initialChildSize: 0.40,
          items: allItemList,
          itemsTextStyle: itemsTextStyle,
          searchHint: widget.hint,
          cancelText: Text(
            widget.cancelText,
            style: cancelButtonTextStyle,
          ),
          confirmText: Text(
            widget.confirmText,
            style: confirmButtonTextStyle,
          ),
          selectedColor: widget.selectedColor,
          unselectedColor: widget.unselectedColor,
          searchable: true,
          decoration: BoxDecoration(
            color: Colors.blue.withOpacity(0.1),
            borderRadius: BorderRadius.all(Radius.circular(12)),
            border: Border.all(
              color: Colors.grey,
              width: 1,
            ),
          ),
          buttonIcon: Icon(
            Icons.list_alt_outlined,
            color: Colors.blue,
          ),
          buttonText: Text(
            widget.hint,
            style: TextStyle(
              color: Colors.grey[600],
              fontSize: 14,
            ),
          ),
          onConfirm: (results) {
            setState(() {
              selectedItems = results;
              widget.addCallback(context, results);
            });
          },
          chipDisplay: MultiSelectChipDisplay(
            chipColor: Colors.lightBlue[100],
            textStyle:
                TextStyle(color: Colors.blue, fontWeight: FontWeight.bold),
            scroll: true,
            onTap: (value) {
              setState(() {
                selectedItems.remove(value);
                widget.removeCallback(context, value);
              });
            },
          ),
        ));
  }
}
