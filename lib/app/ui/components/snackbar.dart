import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

SnackBar genericSnackbar(BuildContext context, String message,
    {Color background = Colors.orangeAccent,
    Color textColor = Colors.black,
    double fontSize = 12,
    FontWeight fontWeight = FontWeight.bold,
    int duration = 1}) {
  double width = message.length * fontSize;
  if (width > MediaQuery.maybeOf(context).size.width)
    width = MediaQuery.maybeOf(context).size.width;
  return SnackBar(
    shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(20))),
    elevation: 1,
    width: width,
    behavior: SnackBarBehavior.floating,
    backgroundColor: background,
    content: Text(message,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: fontSize,
          color: textColor,
          fontWeight: fontWeight,
        )),
    duration: Duration(seconds: duration),
  );
}

void showTopSnackbar(BuildContext context, String title, String message,
    {Color background = Colors.orangeAccent,
    Color titleColor = Colors.red,
    Color msgColor = Colors.black,
    double fontSize = 14,
    FontWeight fontWeight = FontWeight.normal,
    int duration = 1}) {
  double width = message.length * fontSize;
  if (width > MediaQuery.maybeOf(context).size.width)
    width = MediaQuery.maybeOf(context).size.width;
  Get.snackbar(title, message,
      backgroundColor: background,
      snackPosition: SnackPosition.TOP,
      duration: Duration(seconds: duration),
      // maxWidth: width,
      titleText: Text(title,
          textAlign: TextAlign.start,
          style: TextStyle(
            fontSize: fontSize + 2,
            color: titleColor,
            fontWeight: FontWeight.bold,
          )),
      messageText: Text(message,
          textAlign: TextAlign.start,
          style: TextStyle(
            fontSize: fontSize,
            color: msgColor,
            fontWeight: fontWeight,
          )),
      snackStyle: SnackStyle.FLOATING);
}
