import 'package:flutter/material.dart';

class CPAPDataPool extends InheritedWidget {
  final List<dynamic> accountList;
  final List<dynamic> equipmentList;
  final List<dynamic> patientList;
  final List<dynamic> notificationList;
  final int notificationTotalPage;

  CPAPDataPool(
      {Widget child,
      this.accountList,
      this.patientList,
      this.equipmentList,
      this.notificationList,
      this.notificationTotalPage})
      : super(child: child);

  @override
  bool updateShouldNotify(CPAPDataPool oldWidget) {
    if ((oldWidget.accountList != accountList) ||
        (oldWidget.equipmentList != equipmentList) ||
        (oldWidget.notificationList != notificationList) ||
        (oldWidget.notificationTotalPage != notificationTotalPage) ||
        (oldWidget.patientList != patientList)) return true;
    return false;
  }

  static CPAPDataPool of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<CPAPDataPool>();
  }
}
