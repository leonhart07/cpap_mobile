import 'package:flutter/material.dart';

class EditEquipmentChangeNotifier with ChangeNotifier {
  bool _enable = false;
  bool get enable => _enable;

  void checkEnable(dynamic current, dynamic origin) {
    final left = current['booking'] as List;
    final right = origin['booking'] as List;
    _enable = false;
    for (var i = 0; i < left.length; i++) {
      if (left[i]['date_str'].isEmpty || left[i]['content'].isEmpty) {
        notifyListeners();
        return;
      }
    }
    if (Set.from(left) != Set.from(right)) _enable = true;
    notifyListeners();
  }
}
