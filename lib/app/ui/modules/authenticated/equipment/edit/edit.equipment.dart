import 'dart:convert';
import 'package:cpap_mobile/app/ui/components/input_widget.dart';
import 'package:cpap_mobile/app/ui/components/loading.dart';
import 'package:cpap_mobile/app/ui/components/snackbar.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/home/home.viewmodel.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'edit.equipment.notifier.dart';

class EditEquipment extends StatefulWidget {
  EditEquipment({Key key, this.equipment}) : super(key: key);
  final dynamic equipment;
  @override
  _EditEquipmentState createState() => _EditEquipmentState();
}

class _EditEquipmentState extends State<EditEquipment> {
  final vm = inject<HomeViewModel>();
  dynamic equipment;

  @override
  void initState() {
    equipment = json.decode(json.encode(widget.equipment));
    sortBookingList(equipment);
    super.initState();
  }

  int sortBookingList(Map<String, dynamic> equipment) {
    var historyList = equipment['booking'] as List<dynamic>;
    if (historyList != null && historyList.length > 0) {
      if (historyList.length > 0)
        historyList.sort((a, b) {
          var date1 =
              DateFormat('dd/MM/yyyy').parse(a['date_str'] ?? '01/01/1970');
          var date2 =
              DateFormat('dd/MM/yyyy').parse(b['date_str'] ?? '01/01/1970');
          return date2.compareTo(date1);
        });
      return historyList.length;
    }
    return 0;
  }

  void newBooking(BuildContext context) {
    sortBookingList(equipment);
    final booking = equipment['booking'] as List;
    DateTime lastest = DateTime.now();

    if (booking.length > 0) {
      try {
        lastest = DateFormat('dd/MM/yyyy').parse(booking[0]['date_str']);
      } catch (e) {
        lastest = DateTime.now();
      }
      if (booking[0]['date_str'].isEmpty || booking[0]['content'].isEmpty)
        return;
    }
    setState(() {
      booking.insert(0, {
        'content': '',
        'date_str':
            DateFormat('dd/MM/yyyy').format(lastest.add(Duration(days: 1)))
      });
      context
          .read<EditEquipmentChangeNotifier>()
          .checkEnable(equipment, widget.equipment);
    });
  }

  void updateEquipment(BuildContext context) {
    vm
        .editEquipmentBooking(equipment)
        .then((value) => {Navigator.pop(context, value)});
  }

  Widget editEquipmentAction(BuildContext context) {
    bool enable = context.watch<EditEquipmentChangeNotifier>().enable;
    return MaterialButton(
      onPressed: () {
        if (enable) updateEquipment(context);
      },
      child: Text(
        'Lưu',
        style: TextStyle(color: enable ? Colors.blue : Colors.grey),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (_) => EditEquipmentChangeNotifier(),
          ),
        ],
        child: StreamBuilder(
          stream: vm.loading,
          builder: (context, snapshot) {
            return LoadingWidget(
              message: '',
              status: snapshot.data,
              child: SafeArea(
                child: Scaffold(
                  resizeToAvoidBottomInset: false,
                  appBar: AppBar(
                    titleSpacing: 0,
                    centerTitle: true,
                    backgroundColor: Colors.white,
                    title: pageTitleWidget(context, 'Lịch Hẹn',
                        height: 20,
                        additionWiget: [
                          Text(
                            equipment['name'],
                            style: pageTitleTextStyle(color: Colors.red),
                            textAlign: TextAlign.center,
                          )
                        ]),
                    actions: [editEquipmentAction(context)],
                  ),
                  backgroundColor: Colors.white,
                  body: editPageBody(context, equipment),
                  floatingActionButton: new FloatingActionButton(
                      elevation: 0.0,
                      child: new Icon(Icons.add),
                      backgroundColor: Colors.blue,
                      mini: true,
                      onPressed: () {
                        newBooking(context);
                      }),
                  floatingActionButtonLocation:
                      FloatingActionButtonLocation.startFloat,
                ),
              ),
            );
          },
        ));
  }

  Widget editElement(BuildContext context, dynamic equipment, int index) {
    void validateInput() {
      context
          .read<EditEquipmentChangeNotifier>()
          .checkEnable(equipment, widget.equipment);
    }

    dynamic item = equipment['booking'];
    final String message = 'Remove ' + item[index]['date_str'];
    double height = MediaQuery.maybeOf(context).size.height;
    double width = MediaQuery.maybeOf(context).size.width;
    return Dismissible(
        // Show a red background as the item is swiped away.
        background: Container(color: Colors.grey),
        key: Key(item[index]['date_str']),
        direction: DismissDirection.endToStart,
        onDismissed: (direction) {
          if (DismissDirection.endToStart == direction)
            setState(() {
              item.removeAt(index);
              validateInput();
              ScaffoldMessenger.of(context).showSnackBar(genericSnackbar(
                  context, message,
                  background: Colors.orangeAccent[200]));
            });
        },
        child: Container(
            margin: const EdgeInsets.only(
                bottom: 6.0), //Same as `blurRadius` i guess
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.blueAccent,
                  offset: Offset(0.5, 1.0), //(x,y)
                  blurRadius: 1.0,
                ),
              ],
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                createDateTimeWidget(context, 'Ngày', item[index]['date_str'],
                    (value) {
                  item[index]['date_str'] = value;
                  validateInput();
                },
                    radius: 0.0,
                    iconName: '',
                    width: (width - 64) / 3,
                    height: height / 15,
                    passYear: 0,
                    helpText: 'Ngày Hẹn'),
                createTextWidget(context, 'Nội dung', item[index]['content'],
                    (value) {
                  item[index]['content'] = value;
                  validateInput();
                },
                    radius: 0.0,
                    iconName: '',
                    width: (width - 64) / 3 * 2,
                    height: height / 15)
              ],
            )));
  }

  Widget editPageBody(BuildContext context, dynamic equipment) {
    return Stack(
      children: [
        Container(
          width: MediaQuery.maybeOf(context).size.width,
          height: MediaQuery.maybeOf(context).size.height,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/background/main_light.jpg'),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Padding(
            padding: const EdgeInsets.only(
                top: 32.0, left: 4.0, right: 0.0, bottom: 16.0),
            child: Container(
              child: Padding(
                padding: const EdgeInsets.only(top: 8, left: 8, bottom: 8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: ListView.builder(
                        itemCount: equipment['booking'].length,
                        itemBuilder: (context, index) {
                          return editElement(context, equipment, index);
                        },
                      ),
                    )
                  ],
                ),
              ),
            )),
      ],
    );
  }
}
