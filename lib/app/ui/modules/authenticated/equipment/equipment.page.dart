import 'package:cpap_mobile/app/ui/components/input_widget.dart';
import 'package:cpap_mobile/app/ui/components/data_provider.dart';
import 'package:cpap_mobile/app/ui/components/snackbar.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/equipment/add/add.equipment.page.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/home/home.viewmodel.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:cpap_mobile/app/ui/components/expandable_list_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'edit/edit.equipment.dart';

class EquipmentPage extends StatelessWidget {
  const EquipmentPage({Key key, this.fetchData, this.role}) : super(key: key);
  final fetchData;
  final String role;

  void deleteEquipment(dynamic equipment) {
    final vm = inject<HomeViewModel>();
    final equipmentId = equipment['id'] as String;
    if (equipmentId == null || equipmentId.isEmpty) {
      return;
    }

    vm.deleteEquipment(equipmentId).then((value) {
      if (value) fetchData();
    });
  }

  Future<void> editEquipment(BuildContext context, dynamic equipment) async {
    dynamic result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => EditEquipment(
          equipment: equipment,
        ),
      ),
    );
    if (result == true) {
      fetchData();
    }
  }

  Future<void> newEquipment(BuildContext context) async {
    // Navigator.push returns a Future that completes after calling
    // Navigator.pop on the Selection Screen.
    final result = await Navigator.push(
      context,
      // Create the SelectionScreen in the next step.
      MaterialPageRoute(builder: (context) => AddEquipmentPage()),
    );
    if (result == true) {
      fetchData();
    }
  }

  Widget getAvatarImage(Map<String, dynamic> account) {
    var imageAsset = 'assets/svg/ic_equipment.svg';
    return SizedBox(
      child: SvgPicture.asset(imageAsset),
      width: 40,
      height: 40,
    );
  }

  List<Widget> itemPrimaryInfoWidget(Map<String, dynamic> item) {
    return [
      Text(
        item['name'] ?? "",
        style: TextStyle(
          fontSize: 12,
          color: Colors.black,
          fontWeight: FontWeight.bold,
        ),
      )
    ];
  }

  DateTime getCurrenUTCDate() {
    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('dd/MM/yyyy');
    return formatter.parse(formatter.format(now));
  }

  Widget itemSecondaryInfoWidget(Map<String, dynamic> item) {
    final bookingList = item['booking'] as List<dynamic>;
    final utcNow = getCurrenUTCDate();
    final widgetKey = GlobalKey();
    var result = [];
    Color color = Colors.green;
    var tooltipText = 'Busy';

    for (var book in bookingList) {
      var dateObj =
          DateFormat('dd/MM/yyyy').parse(book['date_str'] ?? '01/01/1970');
      result.add(dateObj.compareTo(utcNow));
    }

    if (result.contains(0)) {
      color = Colors.red[600];
      tooltipText = 'Đang Có Hẹn';
    } else if (result.contains(1)) {
      color = Colors.yellow[600];
      tooltipText = 'Có Hẹn';
    } else {
      color = Colors.green;
      tooltipText = 'Trống';
    }

    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Wrap(spacing: 4, children: [
          Container(
            width: 22,
            height: 22,
            child: new Tooltip(
              showDuration: Duration(seconds: 5),
              decoration: BoxDecoration(
                color: Colors.blue[900].withOpacity(0.5),
                borderRadius: const BorderRadius.all(Radius.circular(4)),
              ),
              preferBelow: true,
              verticalOffset: 20,
              key: widgetKey,
              message: tooltipText,
              textStyle: TextStyle(fontSize: 14, color: Colors.white),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: color,
                  elevation: 0.0,
                ),
                child: Text(tooltipText),
                onPressed: () {
                  final dynamic tooltip = widgetKey.currentState;
                  tooltip.ensureTooltipVisible();
                },
              ),
            ),
          )
        ]));
  }

  int getItemChildCount(Map<String, dynamic> item) {
    return sortBookingList(item);
  }

  String getValueFromDictionary(String key, Map<String, dynamic> dict) {
    if (dict.containsKey(key)) return dict[key];
    return '';
  }

  Widget getItemChildInfo(Map<String, dynamic> item, int index) {
    return ListTile(
        title: Text(
            getValueFromDictionary('date_str', item['booking'][index]) ??
                '01/01/1970',
            textScaleFactor: 0.8,
            style: TextStyle(color: Colors.black)),
        trailing: Text(
            getValueFromDictionary('content', item['booking'][index]) ?? '',
            style: TextStyle(color: Colors.black)),
        tileColor: Colors.grey[200]);
  }

  int sortBookingList(Map<String, dynamic> equipment) {
    var bookingList = equipment['booking'] as List<dynamic>;
    if (bookingList != null && bookingList.length > 0) {
      bookingList.sort((a, b) {
        var date1 =
            DateFormat('dd/MM/yyyy').parse(a['date_str'] ?? '01/01/1970');
        var date2 =
            DateFormat('dd/MM/yyyy').parse(b['date_str'] ?? '01/01/1970');
        return date2.compareTo(date1);
      });
      return bookingList.length;
    }
    return 0;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          titleSpacing: 0,
          centerTitle: true,
          leading: (role != 'admin')
              ? RotatedBox(
                  quarterTurns: 2,
                  child: SizedBox(
                    height: 44,
                    width: 44,
                    child: logoutButton(context),
                  ),
                )
              : Text(''),
          backgroundColor: Colors.white,
          title: pageTitleWidget(context, 'Thiết Bị', height: 20),
          actions: [addingButton(context, newEquipment)],
        ),
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            Container(
              width: MediaQuery.maybeOf(context).size.width,
              height: MediaQuery.maybeOf(context).size.height,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/background/main_light.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            RefreshIndicator(
              onRefresh: () {
                return Future.delayed(
                  Duration(seconds: 1),
                  () {
                    int count = fetchData();
                    ScaffoldMessenger.of(context).showSnackBar(genericSnackbar(
                        context, 'Đồng bộ $count thiết bị',
                        background: Colors.greenAccent[200]));
                  },
                );
              },
              child: Padding(
                padding:
                    const EdgeInsets.only(top: 32.0, right: 0.0, bottom: 16.0),
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 8, bottom: 8),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ExpandableListViewScrollable(
                            itemsList: CPAPDataPool.of(context).equipmentList,
                            editItem: editEquipment,
                            deleteItem: deleteEquipment,
                            expandButtonImage: getAvatarImage,
                            itemPrimaryWidget: itemPrimaryInfoWidget,
                            itemSecondaryWidget: itemSecondaryInfoWidget,
                            childItemCount: getItemChildCount,
                            childItemBuilder: getItemChildInfo)
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        // floatingActionButton: addingButton(context, newEquipment),
        // floatingActionButtonLocation: FloatingActionButtonLocation.startFloat,
      ),
    );
  }
}
