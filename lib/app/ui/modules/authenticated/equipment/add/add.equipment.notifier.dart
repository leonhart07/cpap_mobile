import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class AddEquipmentNotifier with ChangeNotifier, DiagnosticableTreeMixin {
  bool _enable = false;
  bool get enable => _enable;
  void checkEnable(equipment) {
    _enable = equipment.name.isNotEmpty;
    notifyListeners();
  }
}
