import 'package:cpap_mobile/app/data/structure/common.dart';
import 'package:cpap_mobile/app/ui/components/input_widget.dart';
import 'package:cpap_mobile/app/ui/components/loading.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/equipment/add/add.equipment.notifier.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/home/home.viewmodel.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddEquipmentPage extends StatelessWidget {
  AddEquipmentPage({Key key}) : super(key: key);
  final vm = inject<HomeViewModel>();
  final equipment = Equipment();

  void addEquipment(BuildContext context) {
    vm.addEquipment(equipment).then((value) => {Navigator.pop(context, value)});
  }

  Widget addEquipmentAction(BuildContext context) {
    bool enable = context.watch<AddEquipmentNotifier>().enable;
    return MaterialButton(
      onPressed: () {
        if (enable) addEquipment(context);
      },
      child: Text(
        'Tạo',
        style: TextStyle(color: enable ? Colors.blue : Colors.grey),
      ),
    );
  }

  Widget form(BuildContext context) {
    void validateInput() {
      context.read<AddEquipmentNotifier>().checkEnable(equipment);
    }

    return Container(
      width: MediaQuery.maybeOf(context).size.width,
      height: MediaQuery.maybeOf(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/background/main_light.jpg'),
          fit: BoxFit.cover,
        ),
      ),
      padding: EdgeInsets.all(12),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 16),
            createTextWidget(context, 'Tên thiết bị', equipment.name, (value) {
              equipment.name = value;
              validateInput();
            }, iconName: 'ic_equipment.svg'),
          ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (_) => AddEquipmentNotifier(),
          ),
        ],
        child: StreamBuilder(
            stream: vm.loading,
            builder: (context, snapshot) {
              return LoadingWidget(
                message: '',
                status: snapshot.data,
                child: SafeArea(
                  child: Scaffold(
                    resizeToAvoidBottomInset: false,
                    appBar: AppBar(
                      titleSpacing: 0,
                      centerTitle: true,
                      backgroundColor: Colors.white,
                      title: pageTitleWidget(
                        context,
                        'Thêm Thiết Bị',
                        height: 20,
                      ),
                      actions: [addEquipmentAction(context)],
                    ),
                    body: Container(
                      child: form(context),
                    ),
                  ),
                ),
              );
            }));
  }
}
