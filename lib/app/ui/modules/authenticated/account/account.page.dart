import 'package:cpap_mobile/app/ui/components/input_widget.dart';
import 'package:cpap_mobile/app/ui/components/snackbar.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/account/add/new.account.page.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/home/home.viewmodel.dart';
import 'package:cpap_mobile/app/ui/components/expandable_list_view.dart';
import 'package:cpap_mobile/app/ui/components/data_provider.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'edit/edit.account.page.dart';

class AccountPage extends StatelessWidget {
  AccountPage(
      {Key key, this.fetchData, this.accountRoles, this.gotoPatientPage})
      : super(key: key);
  final fetchData;
  final accountRoles;
  final gotoPatientPage;

  Future<void> gotoPatientPageCallback(
      BuildContext context, String doctorId) async {
    if (gotoPatientPage != null)
      gotoPatientPage(context, doctorId);
    else
      print("gotoPatientPage is null");
  }

  void gotoPage(BuildContext context, dynamic item) {
    gotoPatientPageCallback(context, item['id']);
  }

  void deleteAccount(dynamic patient) {
    final vm = inject<HomeViewModel>();
    final map = Map.from(patient);
    final accId = map['id'] as String ?? '';
    if (map == null || accId == null || accId.isEmpty) return;
    vm.deleteAccount(accId).then((value) {
      if (value) {
        fetchData();
      }
    });
  }

  Future<void> editAccount(BuildContext context, dynamic account) async {
    final result = await Navigator.push(
      context,
      // Create the SelectionScreen in the next step.
      MaterialPageRoute(
          builder: (context) => EditAccountPage(
                account: account,
              )),
    );
    if (result == true) {
      fetchData();
    }
  }

  Future<void> newAccount(BuildContext context) async {
    // Navigator.push returns a Future that completes after calling
    // Navigator.pop on the Selection Screen.
    final result = await Navigator.push(
      context,
      // Create the SelectionScreen in the next step.
      MaterialPageRoute(
          builder: (context) => NewAccountPage(rolesMap: accountRoles)),
    );
    if (result == true) {
      fetchData();
    }
  }

  String getUsername(dynamic patient) {
    final map = Map.from(patient);
    if (map == null) return '';
    final fullname = map['fullname'] as String ?? '';
    if (fullname.isNotEmpty) return fullname;
    final email = map['email'] as String ?? '';
    if (email.isEmpty) return '';
    final list = email.split('@');
    return list.first;
  }

  String getHospitalName(dynamic patient) {
    final map = Map.from(patient);
    if (map == null) return 'None';
    final name = map['hospital_name'] as String;
    if (name != null && name.isNotEmpty) return name;
    return 'None';
  }

  Widget getAvatarImage(Map<String, dynamic> account) {
    var imageAsset = 'assets/svg/ic_doctor.svg';
    if (account.containsKey('role')) {
      switch (account['role']) {
        case 'admin':
          imageAsset = 'assets/svg/ic_admin.svg';
          break;
        case 'staff':
          imageAsset = 'assets/svg/ic_staff.svg';
          break;
        default:
          break;
      }
    }
    return SizedBox(
      child: SvgPicture.asset(imageAsset),
      width: 40,
      height: 40,
    );
  }

  Widget accountEmailWidget(Map<String, dynamic> account) {
    return Expanded(
      child: Text(account['email'] ?? '',
          textAlign: TextAlign.right,
          style: TextStyle(
            fontSize: 12,
            color: Colors.black,
            fontWeight: FontWeight.bold,
          )),
    );
  }

  List<Widget> accountInfoWidget(Map<String, dynamic> account) {
    return [
      Text(
        getUsername(account),
        style: TextStyle(
          fontSize: 12,
          color: Colors.black,
          fontWeight: FontWeight.bold,
        ),
      ),
      Text(
        getHospitalName(account),
        style: TextStyle(
          fontSize: 12,
          color: Colors.black,
          fontWeight: FontWeight.bold,
        ),
      ),
    ];
  }

  int getPatientsCount(Map<String, dynamic> item) {
    return item['patients'].length;
  }

  Widget getPatientsInfo(Map<String, dynamic> item, int index) {
    return ListTile(
        title: Text(item['patients'][index] ?? '',
            textScaleFactor: 0.8, style: TextStyle(color: Colors.black)),
        tileColor: Colors.grey[200]);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          titleSpacing: 0,
          centerTitle: true,
          leading: RotatedBox(
            quarterTurns: 2,
            child: SizedBox(
              height: 44,
              width: 44,
              child: logoutButton(context),
            ),
          ),
          backgroundColor: Colors.white,
          title: pageTitleWidget(context, 'Tài Khoản', height: 20),
          actions: [addingButton(context, newAccount)],
        ),
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            Container(
              width: MediaQuery.maybeOf(context).size.width,
              height: MediaQuery.maybeOf(context).size.height,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/background/main_light.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            RefreshIndicator(
              onRefresh: () {
                return Future.delayed(
                  Duration(seconds: 1),
                  () {
                    int count = fetchData();
                    ScaffoldMessenger.of(context).showSnackBar(genericSnackbar(
                        context, 'Đồng bộ $count tài khoản',
                        background: Colors.greenAccent[200]));
                  },
                );
              },
              child: Padding(
                padding:
                    const EdgeInsets.only(top: 32.0, right: 4.0, bottom: 16.0),
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 8.0, right: 8.0, bottom: 8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ExpandableListViewScrollable(
                            itemsList: CPAPDataPool.of(context).accountList,
                            editItem: editAccount,
                            deleteItem: deleteAccount,
                            expandButtonImage: getAvatarImage,
                            itemPrimaryWidget: accountInfoWidget,
                            itemSecondaryWidget: accountEmailWidget,
                            childItemCount: getPatientsCount,
                            childItemBuilder: getPatientsInfo,
                            onPressCallback: gotoPage)
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        // floatingActionButton: addingButton(context, newAccount),
        // floatingActionButtonLocation: FloatingActionButtonLocation.startFloat,
      ),
    );
  }
}
