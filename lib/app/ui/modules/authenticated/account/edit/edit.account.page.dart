import 'package:cpap_mobile/app/data/structure/common.dart';
import 'package:cpap_mobile/app/ui/components/input_widget.dart';
import 'package:cpap_mobile/app/ui/components/loading.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/account/edit/edit.account.notifier.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/home/home.viewmodel.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EditAccountPage extends StatelessWidget {
  EditAccountPage({Key key, this.account}) : super(key: key);
  final dynamic account;
  final vm = inject<HomeViewModel>();
  final edittingAccount = Account();

  void editAccount(BuildContext context) {
    vm
        .editAccount(edittingAccount)
        .then((value) => {Navigator.pop(context, value)});
  }

  Widget editAccountWidget(BuildContext context) {
    bool enable = context.watch<EditAccountChangeNotifier>().enable;
    return MaterialButton(
      onPressed: () {
        if (enable) editAccount(context);
      },
      child: Text(
        'Lưu',
        style: TextStyle(color: enable ? Colors.blue : Colors.grey),
      ),
    );
  }

  Widget form(BuildContext context) {
    void validateInput() {
      context
          .read<EditAccountChangeNotifier>()
          .checkEnable(edittingAccount, account);
    }

    return Container(
      width: MediaQuery.maybeOf(context).size.width,
      height: MediaQuery.maybeOf(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/background/main_light.jpg'),
          fit: BoxFit.cover,
        ),
      ),
      padding: EdgeInsets.all(12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 16),
          createTextWidget(context, 'Họ và Tên', edittingAccount.name, (value) {
            edittingAccount.name = value;
            validateInput();
          }, iconName: 'ic_account.svg'),
          SizedBox(height: 16),
          createTextWidget(context, 'Số điện thoại', edittingAccount.phone,
              (value) {
            edittingAccount.phone = value;
            validateInput();
          }, isNumber: true, iconName: 'ic_phone.svg'),
          SizedBox(height: 16),
          createTextWidget(context, 'Địa chỉ', edittingAccount.address,
              (value) {
            edittingAccount.address = value;
            validateInput();
          }, iconName: 'ic_location.svg'),
          SizedBox(height: 16),
          createDateTimeWidget(context, 'Sinh Nhật', edittingAccount.birthday,
              (value) {
            edittingAccount.birthday = value;
            validateInput();
          }),
          if (account['role'] == 'user')
            Column(children: [
              SizedBox(height: 16),
              createTextWidget(context, 'Bệnh Viện', edittingAccount.hospital,
                  (value) {
                edittingAccount.hospital = value;
                validateInput();
              }, iconName: 'ic_hospital.svg'),
              SizedBox(height: 16),
              createTextWidget(context, 'Phòng Khám', edittingAccount.clinic,
                  (value) {
                edittingAccount.clinic = value;
                validateInput();
              }, iconName: 'ic_clinic.svg')
            ]),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    edittingAccount.id = account['id'] ?? '';
    edittingAccount.email = account['email'] ?? '';
    edittingAccount.name = account['fullname'] ?? '';
    edittingAccount.hospital = account['hospital_name'] ?? '';
    edittingAccount.phone = account['phone_number'] ?? '';
    edittingAccount.address = account['address_str'] ?? '';
    edittingAccount.birthday = account['birthday'] ?? '';
    edittingAccount.clinic = account['clinic'] ?? '';

    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (_) => EditAccountChangeNotifier(),
          ),
        ],
        child: StreamBuilder(
          stream: vm.loading,
          builder: (context, snapshot) {
            return LoadingWidget(
              message: '',
              status: snapshot.data,
              child: SafeArea(
                child: Scaffold(
                  appBar: AppBar(
                    titleSpacing: 0,
                    centerTitle: true,
                    backgroundColor: Colors.white,
                    title: pageTitleWidget(context, 'Tài Khoản',
                        height: 20,
                        additionWiget: [
                          Text(
                            account["email"],
                            style: pageTitleTextStyle(color: Colors.red),
                            textAlign: TextAlign.center,
                          )
                        ]),
                    actions: [editAccountWidget(context)],
                  ),
                  body: Container(
                    child: form(context),
                  ),
                ),
              ),
            );
          },
        ));
  }
}
