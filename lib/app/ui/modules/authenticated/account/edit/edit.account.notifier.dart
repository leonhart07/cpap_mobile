import 'package:cpap_mobile/app/data/structure/common.dart';
import 'package:flutter/material.dart';

class EditAccountChangeNotifier with ChangeNotifier {
  bool _enable = false;
  bool get enable => _enable;

  void checkEnable(Account edittingAccount, dynamic origin) {
    _enable = ((edittingAccount.id != origin['id']) ||
        (edittingAccount.email != origin['email']) ||
        (edittingAccount.name != origin['fullname']) ||
        (edittingAccount.hospital != origin['hospital_name']) ||
        (edittingAccount.phone != origin['phone_number']) ||
        (edittingAccount.address != origin['address_str']) ||
        (edittingAccount.birthday != origin['birthday']) ||
        (edittingAccount.clinic != origin['clinic']));
    notifyListeners();
  }
}
