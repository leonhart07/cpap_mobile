import 'dart:collection';
import 'package:cpap_mobile/app/data/structure/common.dart';
import 'package:cpap_mobile/app/data/structure/util.dart';
import 'package:cpap_mobile/app/ui/components/input_widget.dart';
import 'package:cpap_mobile/app/ui/components/loading.dart';
import 'package:cpap_mobile/app/ui/components/snackbar.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/account/add/add.change.notifier.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/home/home.viewmodel.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class NewAccountPage extends StatelessWidget {
  NewAccountPage({Key key, this.rolesMap}) : super(key: key);
  final Map<String, Object> rolesMap;
  final vm = inject<HomeViewModel>();
  final Account account = Account();

  void newAccount(BuildContext context) {
    var msgPass = validatePassword(account.password);
    var msgEmail = validateEmail(account.email);
    FocusScope.of(context).unfocus();
    if (msgPass != null)
      ScaffoldMessenger.of(context).showSnackBar(genericSnackbar(
          context, msgPass,
          duration: 2,
          textColor: Colors.red[800],
          background: Colors.red[100]));
    else {
      if (msgEmail != null)
        ScaffoldMessenger.of(context).showSnackBar(genericSnackbar(
            context, msgEmail,
            duration: 2,
            textColor: Colors.red[800],
            background: Colors.red[100]));
      else
        vm.newAccount(account).then((value) => {Navigator.pop(context, value)});
    }
  }

  Widget newAccountWidget(BuildContext context) {
    bool enable = context.watch<NewAccountChangeNotifier>().enable;
    return MaterialButton(
      onPressed: () {
        if (enable) newAccount(context);
      },
      child: Text(
        'Create',
        style: TextStyle(color: enable ? Colors.blue : Colors.grey),
      ),
    );
  }

  Widget form(BuildContext context, Map<String, Object> roles) {
    void validateInput() {
      context.read<NewAccountChangeNotifier>().checkEnable(account);
    }

    account.role = account.role.isEmpty ? roles.keys.first : account.role;
    return Container(
      width: MediaQuery.maybeOf(context).size.width,
      height: MediaQuery.maybeOf(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/background/main_light.jpg'),
          fit: BoxFit.cover,
        ),
      ),
      padding: EdgeInsets.all(12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 16),
          createTextWidget(context, 'Email', account.email, (value) {
            account.email = value;
            validateInput();
          }, iconName: 'ic_email.svg'),
          SizedBox(height: 16),
          createTextWidget(context, 'Password', account.password, (value) {
            account.password = value;
            validateInput();
          }, isPassword: true, iconName: 'ic_password.svg'),
          SizedBox(height: 16),
          createTextWidget(context, 'Họ và Tên', account.name, (value) {
            account.name = value;
            validateInput();
          }, iconName: 'ic_account.svg'),
          SizedBox(height: 16),
          createTextWidget(context, 'Số điện thoại', account.phone, (value) {
            account.phone = value;
            validateInput();
          }, isNumber: true, iconName: 'ic_phone.svg'),
          SizedBox(height: 16),
          createTextWidget(context, 'Địa chỉ', account.address, (value) {
            account.address = value;
            validateInput();
          }, iconName: 'ic_location.svg'),
          SizedBox(height: 16),
          createDateTimeWidget(context, 'Sinh Nhật', account.birthday, (value) {
            account.birthday = value;
            validateInput();
          }),
          if (account.role == 'user')
            Column(children: [
              SizedBox(height: 16),
              createTextWidget(context, 'Bệnh Viện', account.hospital, (value) {
                account.hospital = value;
                validateInput();
              }, iconName: 'ic_hospital.svg'),
              SizedBox(height: 16),
              createTextWidget(context, 'Phòng Khám', account.clinic, (value) {
                account.clinic = value;
                validateInput();
              }, iconName: 'ic_clinic.svg')
            ]),
          SizedBox(height: 16),
          createListWidget(context, 'Loại Tài Khoản', roles, (value) {
            account.role = value;
            validateInput();
          }),
          // cardSelect(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (_) => NewAccountChangeNotifier(),
          ),
        ],
        child: StreamBuilder(
            stream: vm.loading,
            builder: (context, snapshot) {
              return LoadingWidget(
                message: '',
                status: snapshot.data,
                child: SafeArea(
                  child: Scaffold(
                    resizeToAvoidBottomInset: false,
                    appBar: AppBar(
                      titleSpacing: 0,
                      centerTitle: true,
                      backgroundColor: Colors.white,
                      title: pageTitleWidget(context, 'Thêm Tài Khoản',
                          height: 20),
                      actions: [newAccountWidget(context)],
                    ),
                    body: Container(
                      child: form(
                          context,
                          SplayTreeMap.from(
                              rolesMap,
                              (key1, key2) => rolesMap[key2]
                                  .toString()
                                  .compareTo(rolesMap[key1].toString()))),
                    ),
                  ),
                ),
              );
            }));
  }
}
