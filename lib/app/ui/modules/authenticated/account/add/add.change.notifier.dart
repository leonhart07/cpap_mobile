import 'package:cpap_mobile/app/data/structure/common.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class NewAccountChangeNotifier with ChangeNotifier, DiagnosticableTreeMixin {
  bool _enable = false;
  bool get enable => _enable;
  void checkEnable(Account account) {
    _enable = ((account.email.isNotEmpty) &&
        (account.password.isNotEmpty) &&
        (account.role.isNotEmpty) &&
        (account.phone.isNotEmpty));
    notifyListeners();
  }
}
