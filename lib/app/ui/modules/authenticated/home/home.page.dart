import 'dart:async';
// import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:cpap_mobile/app/data/sources/cache/storage.helper.dart';
import 'package:cpap_mobile/app/data/structure/util.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/change.password/change.password.page.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/account/account.page.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/equipment/equipment.page.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/home/firebase_init.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/home/home.viewmodel.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/patient/patient.page.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/notification/notification.page.dart';
import 'package:cpap_mobile/app/ui/modules/unauthenticated/login/login.page.dart';
import 'package:cpap_mobile/app/ui/components/data_provider.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:cpap_mobile/device/nav/nav_slide_from_top.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:overlay_support/overlay_support.dart';

void showNotificationMessage(Map<String, dynamic> message) async {
  Widget dissmisButton = Text('Dissmiss',
      style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold));
  Widget timeText = Text('Now',
      style: TextStyle(
          fontSize: 10, color: Colors.black, fontWeight: FontWeight.normal));

  if (message.containsKey('notification')) {
    final dynamic notification = message['notification'];
    if (message.containsKey('data')) {
      final dynamic data = message['data'];
      if (data.containsKey('image') && data['image'].length > 0) {
        dissmisButton = SizedBox(
            height: 40, width: 40, child: Image.network(data['image']));
      }
      if (data.containsKey('time') && data['time'].length > 0) {
        var time = stringIsoToLocalDateTime(data['time']);
        if (dayDifference(time, DateTime.now().toLocal()) != 0) {
          timeText = Text(
              '${DateFormat('dd/MM/yyyy').format(time)} ${DateFormat('HH:mm').format(time)}',
              style: TextStyle(
                  fontSize: 10,
                  color: Colors.black,
                  fontWeight: FontWeight.normal));
        } else if (!time.isAtSameMomentAs(DateTime.now().toLocal()))
          timeText = Text('${DateFormat('HH:mm').format(time)}',
              style: TextStyle(
                  fontSize: 10,
                  color: Colors.black,
                  fontWeight: FontWeight.normal));
      }
    }

    showSimpleNotification(
        Text(notification['title'],
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
        leading: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                width: 32,
                height: 32,
                child: Image.asset('assets/ic_app.png'),
              ),
              timeText
            ]),
        subtitle: Text(notification['body'],
            style:
                TextStyle(color: Colors.white, fontWeight: FontWeight.normal)),
        background: Colors.blue,
        autoDismiss: true, trailing: Builder(builder: (context) {
      return ElevatedButton(
          style: ElevatedButton.styleFrom(
            elevation: 0.0,
          ),
          onPressed: () {
            OverlaySupportEntry.of(context).dismiss();
          },
          child: dissmisButton);
    }), duration: Duration(milliseconds: 5000));
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  StreamSubscription iosSubscription;
  Map<String, dynamic> notificationResponse;
  String thisUserRole;
  final apiClient = inject<HomeViewModel>();
  List<dynamic> accountList = [];
  List<dynamic> patientList = [];
  List<dynamic> equipmentList = [];
  List<dynamic> notificationList = [];
  int notificationTotalPage = 1;
  Map<String, Object> allAccRoleMap = {};
  int currentTabIndex = 0;
  List allPages = [];

  Future<void> gotoPatientPage(BuildContext context, String doctorId) async {
    setState(() {
      allPages[1] = PatientPage(
          fetchData: fetchPatientList, role: thisUserRole, doctorId: doctorId);
      currentTabIndex = 1;
    });
  }

  void sortListBy(List<dynamic> itemList, {String key: 'last_update'}) {
    if (itemList != null && itemList.length > 0) {
      itemList.sort((a, b) {
        if (a.containsKey(key) && b.containsKey(key)) {
          try {
            var date1 = DateTime.parse(a[key] ?? '2021-03-05T00:00:00.000000');
            var date2 = DateTime.parse(b[key] ?? '2021-03-05T00:00:00.000000');
            return date2.compareTo(date1);
          } catch (e) {
            print(e);
            return 0;
          }
        } else
          return 0;
      });
    }
  }

  void fetchRole({reload: true}) {
    if (apiClient != null) {
      apiClient.getRoles().then((value) {
        if (value != null && value.length > 0) {
          value.forEach((element) {
            allAccRoleMap[element.name] = element.value;
          });
        }
      });
    }
  }

  int fetchAccountList({bool reload: true}) {
    if (thisUserRole == 'admin' && apiClient != null) {
      apiClient.getRoles().then((value) {
        if (value != null && value.length > 0) {
          value.forEach((element) {
            allAccRoleMap[element.name] = element.value;
          });
        }
      });
      apiClient.getAccounts().then((value) {
        List<dynamic> newData = value;
        sortListBy(newData);
        if (generateMd5(newData.toString()) !=
            generateMd5(accountList.toString())) {
          accountList = value;
          if (reload) setState(() {});
        }
        return accountList.length;
      });
    }
    return accountList.length;
  }

  int fetchPatientList({bool reload: true}) {
    if ((thisUserRole == 'admin' || thisUserRole == 'user') &&
        apiClient != null) {
      apiClient.getPatients().then((value) {
        List<dynamic> newData = value;
        sortListBy(newData);
        if (generateMd5(newData.toString()) !=
            generateMd5(patientList.toString())) {
          patientList = value;
          if (reload) setState(() {});
        }
        return patientList.length;
      });
    }
    return patientList.length;
  }

  int fetchEquipmentList({bool reload: true}) {
    if ((thisUserRole == 'admin' || thisUserRole == 'staff') &&
        apiClient != null) {
      apiClient.getEquipments().then((value) {
        List<dynamic> newData = value;
        sortListBy(newData);
        if (generateMd5(newData.toString()) !=
            generateMd5(equipmentList.toString())) {
          equipmentList = value;
          if (reload) setState(() {});
        }
        return equipmentList.length;
      });
    }
    return equipmentList.length;
  }

  void fetchNotificationMessageList(int page,
      {FetchDataDoneCallBack callback}) {
    List<dynamic> listData = [];
    if (apiClient != null) {
      apiClient.getNotifications(page).then((value) {
        if (value != null) {
          notificationTotalPage = value['total_of_pages'];
          listData = value['notification'];
          sortListBy(listData, key: 'time');
          if (notificationList.length >= page)
            notificationList[page - 1] = listData;
          else
            notificationList.add(listData);
          if (callback != null) callback(listData);
        }
      });
    }
  }

  @override
  void initState() {
    super.initState();
    checkLogin().then((value) {});
    flutterNotificationInit().then((token) {
      StorageHelper.set(StorageKeys.firebaseMessagingToken, token);
      apiClient.sub(token);
    });
  }

  @override
  void dispose() {
    if (iosSubscription != null) iosSubscription.cancel();
    super.dispose();
  }

  Future<void> checkLogin() async {
    final savedToken = await StorageHelper.get(StorageKeys.token);
    final saveRole = await StorageHelper.get(StorageKeys.role);
    thisUserRole = saveRole;
    if (savedToken == null || savedToken.isEmpty) {
      Navigator.pushReplacement(
        context,
        NavSlideFromTop(
          page: LoginPage(),
        ),
      );
    } else {
      fetchRole();
      fetchAccountList();
      fetchPatientList();
      fetchEquipmentList();
      fetchNotificationMessageList(1);
    }

    setState(() {
      allPages = getPages(thisUserRole);
    });
  }

  List<String> getNavigationButtonIconName(String userRole) {
    List<String> iconFileName = [];
    switch (userRole) {
      case 'admin':
        iconFileName.add('assets/svg/ic_account.svg');
        iconFileName.add('assets/svg/ic_patient.svg');
        iconFileName.add('assets/svg/ic_equipment.svg');
        iconFileName.add('assets/svg/ic_notification.svg');
        iconFileName.add('assets/svg/ic_keys.svg');
        break;
      case 'staff':
        iconFileName.add('assets/svg/ic_equipment.svg');
        iconFileName.add('assets/svg/ic_notification.svg');
        iconFileName.add('assets/svg/ic_keys.svg');
        break;
      case 'user':
        iconFileName.add('assets/svg/ic_patient.svg');
        iconFileName.add('assets/svg/ic_notification.svg');
        iconFileName.add('assets/svg/ic_keys.svg');
        break;
      default:
        break;
    }
    return iconFileName;
  }

  List<Widget> getPages(String userRole) {
    List<Widget> pages = [];
    pages.add(AccountPage(
      fetchData: fetchAccountList,
      accountRoles: allAccRoleMap,
      gotoPatientPage: gotoPatientPage,
    ));
    pages.add(PatientPage(fetchData: fetchPatientList, role: userRole));
    pages.add(EquipmentPage(fetchData: fetchEquipmentList, role: userRole));
    pages.add(NotificationPage(
        fetchData: fetchNotificationMessageList, role: userRole));
    pages.add(ChangePasswordPage());

    switch (userRole) {
      case 'admin':
        return pages;
      case 'staff':
        return pages.sublist(2);
      default:
        return [pages[1], pages[3], pages[4]];
    }
  }

  Widget loadingPage() {
    return Stack(
      children: [
        Positioned(
          top: 0,
          child: Container(
            width: MediaQuery.maybeOf(context).size.width,
            height: MediaQuery.maybeOf(context).size.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/background/login.jpg'),
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
        Container(
          width: MediaQuery.maybeOf(context).size.width,
          height: MediaQuery.maybeOf(context).size.height,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 80),
              Container(
                child: SizedBox(
                  width: 86,
                  height: 86,
                  child: Image.asset('assets/ic_app.png'),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget getBottomNavigationBar(String userRole) {
    List<BottomNavigationBarItem> getBottomNavigationItem() {
      List<String> iconFileName = getNavigationButtonIconName(userRole);
      return [
        for (var i = 0; i < iconFileName.length; i += 1)
          BottomNavigationBarItem(
            backgroundColor: Colors.white,
            icon: Container(
                height: 32,
                child: SvgPicture.asset(
                  iconFileName[i],
                  semanticsLabel: '',
                  height: 32,
                )),
            label: '',
            activeIcon: Container(
              child: SvgPicture.asset(
                iconFileName[i],
                semanticsLabel: '',
                height: 44,
              ),
              decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    offset: Offset(0.0, 1.0), //(x,y)
                    blurRadius: 6.0,
                  ),
                ],
              ),
            ),
          ),
      ];
    }

    List tabItemList = getBottomNavigationItem();
    return SizedBox(
        child: BottomNavigationBar(
      selectedIconTheme: IconThemeData(color: Colors.white),
      unselectedIconTheme: IconThemeData(color: Colors.blue[100]),
      elevation: 0.0,
      type: BottomNavigationBarType.fixed,
      currentIndex: currentTabIndex,
      backgroundColor: Colors.blue[100],
      // activeColor: Colors.blue,
      items: (tabItemList.length > 1)
          ? tabItemList
          : [
              for (var i = 0; i < 3; i++)
                BottomNavigationBarItem(
                  label: '',
                  icon: CircularProgressIndicator(),
                )
            ],
      onTap: (int i) => {
        setState(() {
          if (thisUserRole == 'admin' && i == 1)
            allPages[1] =
                PatientPage(fetchData: fetchPatientList, role: thisUserRole);
          currentTabIndex = i;
        })
      },
    ));
  }

  // Widget getConvexAppBar(String userRole) {
  //   List<TabItem> getConvexNavigationIcon() {
  //     List<String> iconFileName = getNavigationButtonIconName(userRole);
  //     return [
  //       for (var i = 0; i < iconFileName.length; i += 1)
  //         TabItem(
  //             icon: SvgPicture.asset(iconFileName[i], semanticsLabel: ''),
  //             title: ''),
  //     ];
  //   }

  //   List tabItemList = getConvexNavigationIcon();
  //   return ConvexAppBar(
  //     style: TabStyle.reactCircle,
  //     controller: DefaultTabController.of(context),
  //     height: 44,
  //     top: -18,
  //     backgroundColor: Colors.blue[100],
  //     // activeColor: Colors.blue,
  //     items: (tabItemList.length > 0)
  //         ? tabItemList
  //         : [
  //             TabItem(
  //               title: '',
  //               icon: CircularProgressIndicator(),
  //             )
  //           ],
  //     initialActiveIndex: 0,
  //     onTap: (int i) => {
  //       setState(() {
  //         currentTabIndex = i;
  //       })
  //     },
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: CPAPDataPool(
            child: (allPages.length > 0)
                ? allPages[currentTabIndex]
                : loadingPage(),
            accountList: accountList,
            patientList: patientList,
            equipmentList: equipmentList,
            notificationList: notificationList,
            notificationTotalPage: notificationTotalPage),
        // bottomNavigationBar: getConvexAppBar(role),
        bottomNavigationBar: getBottomNavigationBar(thisUserRole),
      ),
    );
  }
}
