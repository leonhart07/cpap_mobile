import 'package:cpap_mobile/app/data/account.repository.dart';
import 'package:cpap_mobile/app/data/patient.repository.dart';
import 'package:cpap_mobile/app/data/push.notification.repository.dart';
import 'package:cpap_mobile/app/data/sources/remote/common/http_response.dart';
import 'package:cpap_mobile/app/data/structure/common.dart';
import 'package:cpap_mobile/app/data/equipment.repository.dart';
import 'package:cpap_mobile/app/data/user.repository.dart';
import 'package:cpap_mobile/core/base/view_model.base.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';

class HomeViewModel extends BaseViewModel {
  AccountRepository accRepo = inject<AccountRepository>();
  EquipmentRepository equipRepo = inject<EquipmentRepository>();
  UserRepository userRepo = inject<UserRepository>();
  PatientRepository patientRepo = inject<PatientRepository>();
  PushNotificationRepository pnsRepo = inject<PushNotificationRepository>();

  @override
  void clear() {}

  //==============================================
  //                PATIENT
  //==============================================
  Future<List<dynamic>> getPatients() {
    return patientRepo.getPatients();
  }

  Future<bool> addPatient(Patient patient) {
    return patientRepo.addPatient(patient);
  }

  Future<bool> deletePatient(String patientId) {
    return patientRepo.deletePatient(patientId);
  }

  Future<bool> editPatient(dynamic patient, PatientEditType infoType) {
    return patientRepo.editPatient(patient, infoType);
  }

  //==============================================
  //                USER
  //==============================================
  Future<List<Role>> getRoles() {
    return userRepo.getRoles();
  }

  Future<HttpResponse> changePassword(String oldPass, String newPass) {
    return userRepo.changePassword(oldPass, newPass);
  }

  Future<HttpResponse> forgotPassword(String email) async {
    return userRepo.forgotPassword(email);
  }

  //==============================================
  //                EQUIPMENT
  //==============================================
  Future<List<dynamic>> getEquipments() {
    return equipRepo.getEquipments();
  }

  Future<bool> addEquipment(Equipment equipment) {
    return equipRepo.addEquipment(equipment);
  }

  Future<bool> deleteEquipment(String equipmentId) {
    return equipRepo.deleteEquipment(equipmentId);
  }

  Future<bool> editEquipmentBooking(dynamic equipment) {
    return equipRepo.editEquipmentBooking(equipment);
  }

  //==============================================
  //                ACCOUNT
  //==============================================
  Future<List<dynamic>> getAccounts() {
    return accRepo.getAccounts();
  }

  Future<bool> newAccount(Account account) {
    return accRepo.newAccount(account);
  }

  Future<bool> editAccount(Account account) {
    return accRepo.editAccount(account);
  }

  Future<bool> deleteAccount(String email) {
    return accRepo.deleteAccount(email);
  }

  //==============================================
  //                PUSH NOTITFICATION
  //==============================================
  Future<void> sub(String fcmToken) {
    return pnsRepo.sub(fcmToken);
  }

  Future<bool> unsub(String fcmToken) {
    return pnsRepo.unsub(fcmToken);
  }

  Future<bool> addNotification(NotificationPost dataPost) {
    return pnsRepo.addNotification(dataPost);
  }

  Future<Map<String, dynamic>> getNotifications(int pageNo) {
    return pnsRepo.getNotifications(pageNo);
  }
}
