import 'package:cpap_mobile/app/data/structure/util.dart';
import 'package:cpap_mobile/app/ui/components/data_provider.dart';
import 'package:cpap_mobile/app/ui/components/expandable_list_view.dart';
import 'package:cpap_mobile/app/ui/components/input_widget.dart';
import 'package:cpap_mobile/app/ui/components/snackbar.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/notification/add/add.notification.page.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/notification/webview.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:platform_action_sheet/platform_action_sheet.dart';

class NotificationContentPage extends StatelessWidget {
  const NotificationContentPage(
      {Key key,
      this.notificationList,
      this.doctorId = '',
      this.doctorListByName,
      this.screenWidth})
      : super(key: key);

  final List<dynamic> notificationList;
  final String doctorId;
  final Map<String, Object> doctorListByName;
  final double screenWidth;

  List<dynamic> filterData(List<dynamic> origin, Map<String, String> criteria) {
    List<dynamic> result = [];
    criteria.forEach((k, v) {
      // result.addAll(origin.where((data) => data[k].keys.toList().contains(v)).toList());
      result.addAll(origin.where((data) {
        if (data[k] == null) return false;
        return data[k].keys.toList().contains(v);
      }).toList());
    });
    return result;
  }

  void deleteItem(dynamic item) {}

  Future<void> editItem(BuildContext context, dynamic item) async {}

  void onLongPressCallback(BuildContext context, dynamic item) {
    final String text = item['body'] ?? "";
    List<String> links = [];
    if (text.isNotEmpty) {
      RegExp exp =
          new RegExp(r'(?:(?:https?|ftp):\/\/)?[\w/\-?=%.]+\.[\w/\-?=%.]+');
      Iterable<RegExpMatch> matches = exp.allMatches(text);
      matches.forEach((match) {
        // print(text.substring(match.start, match.end));
        links.add(text.substring(match.start, match.end));
      });
      links.add("Copy");
      PlatformActionSheet().displaySheet(
        context: context,
        title: Row(
          children: <Widget>[
            SizedBox(
              child: Image.asset('assets/logo.png'),
              width: 24,
              height: 24,
            ),
            Text("CPAP"),
          ],
        ),
        actions: links
            .map(
              (e) => ActionSheetAction(
                text: e,
                onPressed: () => {
                  Navigator.pop(context),
                  if (e == 'Copy')
                    {
                      Clipboard.setData(new ClipboardData(text: text)).then(
                        (result) {
                          ScaffoldMessenger.of(context).showSnackBar(
                              genericSnackbar(context, 'Copied to Clipboard',
                                  background: Colors.orangeAccent[200]));
                        },
                      )
                    }
                  else
                    {
                      // open link
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => WebViewContainer(e),
                        ),
                      )
                    }
                },
              ),
            )
            .toList(),
      );
    }
  }

  Widget getAvatarImage(Map<String, dynamic> item) {
    var imageAsset = 'assets/svg/ic_message.svg';
    String imageUrl;

    if (item != null && item.containsKey('image')) imageUrl = item['image'];
    if (imageUrl == null) imageUrl = '';

    return imageUrl.isNotEmpty
        ? SizedBox(height: 44, width: 40, child: Image.network(imageUrl))
        : SizedBox(
            child: SvgPicture.asset(imageAsset),
            height: 22,
            width: 40,
          );
  }

  List<Widget> itemPrimaryInfoWidget(Map<String, dynamic> item) {
    String title = item['title'] ?? "";
    String body = item['body'] ?? "";

    return [
      SizedBox(
          width: screenWidth - (66 + 40 + 68),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title.trimLeft(),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 12,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                if (body.length > 0)
                  Padding(
                      padding: EdgeInsets.only(top: 4),
                      child: Text(
                        body.trimLeft(),
                        maxLines: 5,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.black,
                          fontWeight: FontWeight.normal,
                        ),
                      ))
              ]))
    ];
  }

  Widget itemSecondaryInfoWidget(Map<String, dynamic> item) {
    String firstLine = '';
    String secondLine = '';
    try {
      var time = stringIsoToLocalDateTime(item['time']);
      if (dayDifference(time, DateTime.now().toLocal()) != 0) {
        firstLine = DateFormat('dd/MM/yyyy').format(time);
        secondLine = DateFormat('HH:mm').format(time);
      } else
        firstLine = DateFormat('HH:mm').format(time);
    } catch (e) {
      firstLine = '';
      secondLine = '';
    }

    return Container(
      width: 66,
      child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              firstLine,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontSize: 12,
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
            if (secondLine.length > 0)
              Text(
                secondLine,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.black,
                  fontWeight: FontWeight.normal,
                ),
              )
          ]),
    );
  }

  int getItemChildCount(Map<String, dynamic> item) {
    if (item.containsKey('receiver')) {
      return item['receiver'].keys.toList().length;
    }
    return 0;
  }

  Widget getItemChildInfo(Map<String, dynamic> item, int index) {
    Color textColor = Colors.green;
    if (item['receiver'].values.toList()[index] != 'Sent')
      textColor = Colors.red;
    return ListTile(
        title: Text(
            doctorListByName[item['receiver'].keys.toList()[index]] ?? '',
            textScaleFactor: 0.8,
            style: TextStyle(color: Colors.black)),
        trailing: Text(item['receiver'].values.toList()[index] ?? '',
            textScaleFactor: 0.8, style: TextStyle(color: textColor)),
        tileColor: Colors.grey[200]);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: MediaQuery.maybeOf(context).size.width,
          height: MediaQuery.maybeOf(context).size.height,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/background/main_light.jpg'),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 32.0, right: 0.0, bottom: 16.0),
          child: Container(
            child: Padding(
              padding: const EdgeInsets.only(top: 8, bottom: 8),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ExpandableListViewScrollable(
                      itemsList: doctorId == 'all'
                          ? notificationList
                          : filterData(
                              notificationList, {'receiver': doctorId}),
                      editable: false,
                      editItem: editItem,
                      deleteItem: deleteItem,
                      expandButtonImage: getAvatarImage,
                      itemPrimaryWidget: itemPrimaryInfoWidget,
                      itemSecondaryWidget: itemSecondaryInfoWidget,
                      childItemCount: getItemChildCount,
                      childItemBuilder: getItemChildInfo,
                      onLongPressCallback: onLongPressCallback)
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class NotificationPage extends StatefulWidget {
  const NotificationPage({Key key, this.fetchData, this.role})
      : super(key: key);
  final FetchDataRemoteCall fetchData;
  final String role;
  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  String doctorId = 'all';
  Map<String, Object> doctorListByName;
  int currentPageIndex = 0;
  final PageController pageController = PageController(
    initialPage: 0,
  );

  // @override
  // void dispose() {
  //   pageController.dispose();
  //   super.dispose();
  // }

  void onRefreshDataDone(dynamic data) {
    setState(() {});
  }

  Map<String, Object> getDoctorsListByName(String key, String value,
      {bool all: false, bool empty: false}) {
    Map<String, Object> result = {};
    if (all) result['all'] = 'Tất Cả';
    if (empty) result[''] = 'Không Set';
    List<dynamic> doctors = CPAPDataPool.of(context)
        .accountList
        .where((element) => element['role'] == 'user')
        .toList();
    doctors.forEach((element) {
      result[element[key]] = element[value];
    });
    return result;
  }

  Future<void> newMessage(BuildContext context) async {
    final result = await Navigator.push(
      context,
      // Create the SelectionScreen in the next step.
      MaterialPageRoute(
          builder: (context) => AddNotificationPage(
              doctors: getDoctorsListByName('id', 'fullname'))),
      // AddNotificationPage()),
    );
    if (result == true) {
      widget.fetchData(currentPageIndex + 1, callback: onRefreshDataDone);
    }
  }

  @override
  Widget build(BuildContext context) {
    doctorListByName = getDoctorsListByName('id', 'fullname', all: true);
    return SafeArea(
        child: Scaffold(
            appBar: AppBar(
              titleSpacing: 0,
              centerTitle: true,
              backgroundColor: Colors.white,
              leadingWidth: 96,
              leading: Container(
                width: 96,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: List.generate(3, (i) {
                    return CircleAvatar(
                      radius: 10,
                      backgroundColor: 1 == i ? Colors.red : Colors.amber,
                      child: Text(
                        (currentPageIndex + i - 1 < 0)
                            ? '<'
                            : (currentPageIndex + i - 1 >=
                                    CPAPDataPool.of(context)
                                        .notificationTotalPage)
                                ? '>'
                                : '${currentPageIndex + i - 1}',
                        style: TextStyle(fontSize: 14),
                      ),
                    );
                  }).toList(),
                ),
              ),
              title: pageTitleWidget(context, 'Thông Báo',
                  height: 20,
                  additionWiget: (widget.role == 'admin')
                      ? [
                          createListWidget(context, 'Bác Sĩ', doctorListByName,
                              (value) {
                            setState(() {
                              doctorId = value;
                            });
                          },
                              initValue: doctorId,
                              textStyle: pageTitleTextStyle(
                                  color: Colors.red[800], fontSize: 14),
                              textAlign: TextAlign.left,
                              radius: 0,
                              height: 20,
                              width:
                                  (MediaQuery.maybeOf(context).size.width / 2))
                        ]
                      : null),
              actions: [
                (widget.role == 'admin')
                    ? addingButton(context, newMessage)
                    : Text('')
              ],
            ),
            resizeToAvoidBottomInset: false,
            backgroundColor: Colors.white,
            body: PageView.builder(
              controller: pageController,
              onPageChanged: (int pageNo) {
                setState(() {
                  currentPageIndex =
                      pageNo % (CPAPDataPool.of(context).notificationTotalPage);
                });
              },
              // itemCount: pageCount,
              itemBuilder: (context, position) {
                if (position >= CPAPDataPool.of(context).notificationTotalPage)
                  return null;
                if (position >=
                    CPAPDataPool.of(context).notificationList.length) {
                  widget.fetchData(position + 1, callback: onRefreshDataDone);
                }

                return RefreshIndicator(
                    onRefresh: () {
                      widget.fetchData(position + 1,
                          callback: onRefreshDataDone);
                      return Future.delayed(
                        Duration(seconds: 1),
                        () {
                          ScaffoldMessenger.of(context).showSnackBar(
                              genericSnackbar(
                                  context,
                                  CPAPDataPool.of(context)
                                              .notificationList
                                              .length >
                                          position
                                      ? 'Đồng bộ ${CPAPDataPool.of(context).notificationList[position].length} thông báo'
                                      : 'Không có thông báo mới',
                                  background: Colors.greenAccent[200]));
                        },
                      );
                    },
                    child: NotificationContentPage(
                        notificationList:
                            CPAPDataPool.of(context).notificationList.length >
                                    position
                                ? CPAPDataPool.of(context)
                                    .notificationList[position]
                                : [],
                        doctorId: doctorId,
                        doctorListByName: doctorListByName,
                        screenWidth: MediaQuery.maybeOf(context).size.width));
              },
              // floatingActionButton: SmoothPageIndicator(
              //   controller: pageController,
              //   count: CPAPDataPool.of(context).notificationTotalPage,
              //   effect: JumpingDotEffect(
              //       activeDotColor: Colors.red, dotColor: Colors.amber),
              // ),
            )));
  }
}
