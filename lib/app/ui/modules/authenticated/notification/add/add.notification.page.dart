import 'dart:collection';
import 'package:cpap_mobile/app/data/structure/common.dart';
import 'package:cpap_mobile/app/ui/components/loading.dart';
import 'package:cpap_mobile/app/ui/components/snackbar.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/home/home.viewmodel.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/notification/add/add.change.notifier.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cpap_mobile/app/ui/components/input_widget.dart';
import 'package:provider/provider.dart';

class AddNotificationPage extends StatefulWidget {
  final Map<String, Object> doctors;
  AddNotificationPage({Key key, this.doctors}) : super(key: key);
  @override
  _AddNotificationPageState createState() => _AddNotificationPageState();
}

class _AddNotificationPageState extends State<AddNotificationPage> {
  final vm = inject<HomeViewModel>();
  NotificationPost notif = NotificationPost();
  Map<String, Object> selectedDoctor = {};

  @override
  void initState() {
    super.initState();
  }

  void addNotification(BuildContext context) {
    FocusScope.of(context).unfocus();
    if (notif.title.isEmpty)
      ScaffoldMessenger.of(context).showSnackBar(genericSnackbar(
          context, 'Tiêu Đề Không Thể Trống',
          duration: 2,
          textColor: Colors.red[800],
          background: Colors.red[100]));
    else {
      if (notif.accountID.length <= 0)
        ScaffoldMessenger.of(context).showSnackBar(genericSnackbar(
            context, 'Người Nhận Không Thể Trống',
            duration: 2,
            textColor: Colors.red[800],
            background: Colors.red[100]));
      else {
        vm.addNotification(notif).then((value) => {
              Navigator.pop(
                context,
                value,
              )
            });
      }
    }
  }

  Widget actionButton(BuildContext context, {String text: "Send"}) {
    bool enable = context.watch<NewNotificationChangeNotifier>().enable;
    return MaterialButton(
      onPressed: () {
        if (enable) addNotification(context);
      },
      child: Text(
        'Create',
        style: TextStyle(color: enable ? Colors.blue : Colors.grey),
      ),
    );
  }

  Widget form(BuildContext context, Map<String, Object> accountInfo) {
    void validateInput() {
      context.read<NewNotificationChangeNotifier>().checkEnable(notif);
    }

    void addDestination(BuildContext context, List<dynamic> values) {
      notif.accountID = values.map((e) => e.toString()).toList();
      validateInput();
    }

    void removeDestination(BuildContext context, dynamic value) {
      notif.accountID.remove(value.toString());
      validateInput();
    }

    return Container(
      width: MediaQuery.maybeOf(context).size.width,
      height: MediaQuery.maybeOf(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/background/main_light.jpg'),
          fit: BoxFit.cover,
        ),
      ),
      padding: EdgeInsets.all(12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 16),
          createTextWidget(context, 'Tiêu Đề', notif.title, (value) {
            notif.title = value;
            validateInput();
          },
              iconName: 'ic_title.svg',
              textLength: 256,
              textLine: 10,
              height: 50),
          SizedBox(height: 16),
          createTextWidget(context, 'Hình Minh Họa', notif.image, (value) {
            notif.image = value;
            validateInput();
          },
              iconName: 'ic_image.svg',
              textLength: 1024,
              textLine: 10,
              height: 100),
          SizedBox(height: 16),
          createTextWidget(context, 'Nội Dung', notif.body, (value) {
            notif.body = value;
            validateInput();
          },
              iconName: 'ic_document.svg',
              textLength: 512,
              textLine: 10,
              height: 200),
          SizedBox(height: 16),
          createMultiSelectWidget(
              context, widget.doctors, addDestination, removeDestination,
              hint: 'Bác Sĩ', confirmText: 'Chọn', cancelText: 'Hủy'),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (_) => NewNotificationChangeNotifier(),
          ),
        ],
        child: StreamBuilder(
            stream: vm.loading,
            builder: (context, snapshot) {
              return LoadingWidget(
                message: '',
                status: snapshot.data,
                child: SafeArea(
                  child: Scaffold(
                    resizeToAvoidBottomInset: false,
                    appBar: AppBar(
                      titleSpacing: 0,
                      centerTitle: true,
                      backgroundColor: Colors.white,
                      title:
                          pageTitleWidget(context, 'Tạo Thông Báo', height: 20),
                      actions: [actionButton(context)],
                    ),
                    body: Container(
                      child: form(
                          context,
                          SplayTreeMap.from(
                              widget.doctors,
                              (key1, key2) => widget.doctors[key2]
                                  .toString()
                                  .compareTo(widget.doctors[key1].toString()))),
                    ),
                  ),
                ),
              );
            }));
  }
}
