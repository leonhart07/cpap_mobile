import 'package:cpap_mobile/app/data/structure/common.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class NewNotificationChangeNotifier
    with ChangeNotifier, DiagnosticableTreeMixin {
  bool _enable = false;
  bool get enable => _enable;
  void checkEnable(NotificationPost notif) {
    if (notif.title != null && notif.accountID != null)
      _enable = ((notif.title.isNotEmpty) && (notif.accountID.length > 0));
    notifyListeners();
  }
}
