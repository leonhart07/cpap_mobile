import 'package:cpap_mobile/app/data/structure/common.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class AddPatientNotifier with ChangeNotifier, DiagnosticableTreeMixin {
  bool _enable = false;
  bool get enable => _enable;

  void checkEnable(Patient patient) {
    _enable = ((patient.email.isNotEmpty || patient.phone.isNotEmpty) &&
        (patient.fullname.isNotEmpty));
    notifyListeners();
  }
}
