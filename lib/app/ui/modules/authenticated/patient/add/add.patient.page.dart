import 'package:cpap_mobile/app/data/structure/common.dart';
import 'package:cpap_mobile/app/data/structure/util.dart';
import 'package:cpap_mobile/app/ui/components/input_widget.dart';
import 'package:cpap_mobile/app/ui/components/loading.dart';
import 'package:cpap_mobile/app/ui/components/snackbar.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/home/home.viewmodel.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/patient/add/add.patient.notifier.dart';

import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddPatientPage extends StatelessWidget {
  AddPatientPage({Key key, this.doctors}) : super(key: key);
  final Map<String, Object> doctors;
  final patient = Patient();
  final vm = inject<HomeViewModel>();

  void newPatient(BuildContext context) {
    if (patient.email.isNotEmpty) {
      var msgEmail = validateEmail(patient.email);
      FocusScope.of(context).unfocus();
      if (msgEmail != null) {
        ScaffoldMessenger.of(context).showSnackBar(genericSnackbar(
            context, msgEmail,
            duration: 2,
            textColor: Colors.red[800],
            background: Colors.red[100]));
        return;
      }
    }
    vm.addPatient(patient).then((value) => {Navigator.pop(context, value)});
  }

  Widget addPatientAction(BuildContext context) {
    bool isEnable = context.watch<AddPatientNotifier>().enable;
    return MaterialButton(
      onPressed: () {
        if (isEnable) newPatient(context);
      },
      child: Text(
        'Tạo',
        style: TextStyle(color: isEnable ? Colors.blue : Colors.grey),
      ),
    );
  }

  Widget form(BuildContext context) {
    void validateInput() {
      context.read<AddPatientNotifier>().checkEnable(patient);
    }

    patient.doctorId =
        patient.doctorId.isEmpty ? doctors.keys.first : patient.doctorId;
    return Container(
      width: MediaQuery.maybeOf(context).size.width,
      height: MediaQuery.maybeOf(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/background/main_light.jpg'),
          fit: BoxFit.cover,
        ),
      ),
      padding: EdgeInsets.all(12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 16),
          createTextWidget(context, 'Email', patient.email, (value) {
            patient.email = value;
            validateInput();
          }, iconName: 'ic_email.svg'),
          SizedBox(height: 16),
          createTextWidget(context, 'Họ Và Tên', patient.fullname, (value) {
            patient.fullname = value;
            validateInput();
          }, iconName: 'ic_patient.svg'),
          SizedBox(height: 16),
          createTextWidget(context, 'Số Điện Thoại', patient.phone, (value) {
            patient.phone = value;
            validateInput();
          }, iconName: 'ic_phone.svg', isNumber: true),
          SizedBox(height: 16),
          createDateTimeWidget(context, 'Sinh Nhật', patient.birthday, (value) {
            patient.birthday = value;
            validateInput();
          }),
          SizedBox(
            height: 16,
          ),
          createTextWidget(context, 'Địa chỉ', patient.address, (value) {
            patient.address = value;
            validateInput();
          }, iconName: 'ic_location.svg'),
          SizedBox(
            height: 16,
          ),
          createListWidget(context, 'Loại Tài Khoản', doctors, (value) {
            patient.doctorId = value;
            validateInput();
          }),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (_) => AddPatientNotifier(),
          ),
        ],
        child: StreamBuilder(
            stream: vm.loading,
            builder: (context, snapshot) {
              return LoadingWidget(
                message: '',
                status: snapshot.data,
                child: SafeArea(
                  child: Scaffold(
                    resizeToAvoidBottomInset: false,
                    appBar: AppBar(
                      titleSpacing: 0,
                      centerTitle: true,
                      backgroundColor: Colors.white,
                      title: pageTitleWidget(
                        context,
                        'Thêm Khách Hàng',
                        height: 20,
                      ),
                      actions: [addPatientAction(context)],
                    ),
                    body: Container(
                      child: form(context),
                    ),
                  ),
                ),
              );
            }));
  }
}
