import 'package:cpap_mobile/app/ui/modules/authenticated/home/home.viewmodel.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/patient/edit/edit.patient.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:cpap_mobile/app/ui/components/input_widget.dart';
import 'package:cpap_mobile/app/ui/components/data_provider.dart';
import 'package:cpap_mobile/app/ui/components/snackbar.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/patient/add/add.patient.page.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:cpap_mobile/app/ui/components/expandable_list_view.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PatientPage extends StatefulWidget {
  const PatientPage({Key key, this.fetchData, this.role, this.doctorId})
      : super(key: key);
  final fetchData;
  final String role;
  final String doctorId;

  @override
  _PatientPageState createState() => _PatientPageState();
}

class _PatientPageState extends State<PatientPage> {
  String doctorId;

  List<dynamic> filterData(List<dynamic> origin, Map<String, String> criteria) {
    List<dynamic> result = [];
    criteria.forEach((k, v) {
      result.addAll(origin.where((data) => data[k] == v).toList());
    });
    return result;
  }

  @override
  void initState() {
    doctorId = widget.doctorId ?? '';
    super.initState();
  }

  void deletePatient(dynamic patient) {
    final vm = inject<HomeViewModel>();
    final patientId = patient['id'] as String;
    if (patientId == null || patientId.isEmpty) {
      return;
    }
    vm.deletePatient(patientId).then((value) {
      if (value) widget.fetchData();
    });
  }

  Future<void> editPatient(BuildContext context, dynamic patient) async {
    dynamic result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => EditPatientPage(
            patient: patient,
            doctors: getDoctorsListByName('id', 'fullname', empty: true)),
      ),
    );

    if (result == true) {
      widget.fetchData();
    }
  }

  Map<String, Object> getDoctorsListByName(String key, String value,
      {bool all: false, bool empty: false}) {
    Map<String, Object> result = {};
    if (all) result['all'] = 'Tất Cả';
    if (empty) result[''] = 'Không Set';
    List<dynamic> doctors = CPAPDataPool.of(context)
        .accountList
        .where((element) => element['role'] == 'user')
        .toList();
    doctors.forEach((element) {
      result[element[key]] = element[value];
    });
    return result;
  }

  Future<void> newPatient(BuildContext context) async {
    final result = await Navigator.push(
      context,
      // Create the SelectionScreen in the next step.
      MaterialPageRoute(
          builder: (context) => AddPatientPage(
              doctors: getDoctorsListByName('id', 'fullname', empty: true))),
    );
    if (result == true) {
      widget.fetchData();
    }
  }

  Widget getAvatarImage(Map<String, dynamic> account) {
    var imageAsset = 'assets/svg/ic_patient.svg';
    return SizedBox(
      child: SvgPicture.asset(imageAsset),
      width: 40,
      height: 40,
    );
  }

  List<Widget> itemPrimaryInfoWidget(Map<String, dynamic> item) {
    return [
      Text(
        item['fullname'] ?? "",
        style: TextStyle(
          fontSize: 12,
          color: Colors.black,
          fontWeight: FontWeight.bold,
        ),
      ),
      Text(
        item['phone_number'] ?? '',
        style: TextStyle(
          fontSize: 12,
          color: Colors.black,
          fontWeight: FontWeight.bold,
        ),
      )
    ];
  }

  Widget itemSecondaryInfoWidget(Map<String, dynamic> item) {
    var tooltipList = [for (var i = 0; i < 6; i += 1) GlobalKey()];
    var colorList = [
      [Colors.orange[600], Colors.orange[50]],
      [Colors.orange[600], Colors.orange[50]],
      [Colors.yellow[600], Colors.yellow[50]],
      [Colors.yellow[600], Colors.yellow[50]],
      [Colors.yellow[600], Colors.yellow[50]],
      [Colors.green, Colors.green[50]]
    ];

    Container patientStatusLight(
        GlobalKey key, dynamic statusObject, List<Color> color) {
      String dateStr =
          statusObject['date_str'] != null ? statusObject['date_str'] : '';
      String content =
          statusObject['content'] != null ? statusObject['content'] : '';
      bool enable = dateStr.isNotEmpty && content.isNotEmpty;
      String message = '${statusObject['hint']}: $dateStr\n$content';
      return Container(
        width: 22,
        height: 22,
        child: new Tooltip(
          showDuration: Duration(seconds: 5),
          decoration: BoxDecoration(
            color:
                enable ? color[0].withOpacity(0.9) : color[1].withOpacity(0.9),
            borderRadius: const BorderRadius.all(Radius.circular(12)),
          ),
          preferBelow: true,
          verticalOffset: 20,
          key: key,
          message: message,
          textStyle: TextStyle(
              fontSize: 14, color: Colors.white, fontWeight: FontWeight.w900),
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              primary: enable ? color[0] : color[1],
              elevation: 0.0,
            ),
            child: Text(statusObject['hint']),
            onPressed: () {
              if (enable) {
                final dynamic tooltip = key.currentState;
                tooltip.ensureTooltipVisible();
              }
            },
            onLongPress: () {},
          ),
        ),
      );
    }

    List statList = item['status'] as List<dynamic>;
    if (statList.length > 6)
      statList.sort((a, b) {
        var idx1 = a['stat_id'];
        var idx2 = b['stat_id'];
        return idx1.compareTo(idx2);
      });
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Wrap(spacing: 4, children: [
          for (var i = 0; i < 6; i += 1)
            patientStatusLight(tooltipList[i], statList[i], colorList[i])
        ]));
  }

  int getItemChildCount(Map<String, dynamic> item) {
    return sortHistoryList(item);
  }

  String getValueFromDictionary(String key, Map<String, dynamic> dict) {
    if (dict.containsKey(key)) return dict[key];
    return '';
  }

  Widget getItemChildInfo(Map<String, dynamic> item, int index) {
    return ListTile(
        title: Text(
            getValueFromDictionary('date_str', item['history'][index]) ??
                '01/01/1970',
            textScaleFactor: 0.8,
            style: TextStyle(color: Colors.blue)),
        // subtitle: Text(
        //     getValueFromDictionary('hint', item['history'][index]) ?? '',
        //     textScaleFactor: 0.7,
        //     style: TextStyle(color: Colors.blueAccent)),
        trailing: Text(
            getValueFromDictionary('content', item['history'][index]) ?? '',
            style: TextStyle(color: Colors.black)),
        tileColor: Colors.grey[200]);
  }

  int sortHistoryList(Map<String, dynamic> patient) {
    final int startIndex = 0;
    var historyList = patient['history'] as List<dynamic>;
    if (historyList != null && historyList.length > 0) {
      if (historyList.length > startIndex)
        historyList.sublist(startIndex).sort((a, b) {
          var date1 =
              DateFormat('dd/MM/yyyy').parse(a['date_str'] ?? '01/01/1970');
          var date2 =
              DateFormat('dd/MM/yyyy').parse(b['date_str'] ?? '01/01/1970');
          return date2.compareTo(date1);
        });
      return historyList.length;
    }
    return 0;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          titleSpacing: 0,
          centerTitle: true,
          leading: (widget.role != 'admin')
              ? RotatedBox(
                  quarterTurns: 2,
                  child: SizedBox(
                    height: 44,
                    width: 44,
                    child: logoutButton(context),
                  ),
                )
              : Text(''),
          backgroundColor: Colors.white,
          title: pageTitleWidget(
              context, (widget.role == 'user') ? 'Bệnh Nhân' : 'Khách Hàng',
              height: 20,
              additionWiget: (widget.role == 'admin')
                  ? [
                      createListWidget(context, 'Bác Sĩ',
                          getDoctorsListByName('id', 'fullname', all: true),
                          (value) {
                        setState(() {
                          doctorId = value;
                        });
                      },
                          initValue: doctorId,
                          textStyle: pageTitleTextStyle(
                              color: Colors.red[800], fontSize: 14),
                          textAlign: TextAlign.left,
                          radius: 0,
                          height: 20,
                          width: (MediaQuery.maybeOf(context).size.width / 2))
                    ]
                  : null),
          actions: [
            (widget.role == 'admin')
                ? addingButton(context, newPatient)
                : Text('')
          ],
        ),
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            Container(
              width: MediaQuery.maybeOf(context).size.width,
              height: MediaQuery.maybeOf(context).size.height,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/background/main_light.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            RefreshIndicator(
              onRefresh: () {
                return Future.delayed(
                  Duration(seconds: 1),
                  () {
                    int count = widget.fetchData();
                    ScaffoldMessenger.of(context).showSnackBar(genericSnackbar(
                        context, 'Đồng bộ $count khách hàng',
                        background: Colors.greenAccent[200]));
                  },
                );
              },
              child: Padding(
                padding:
                    const EdgeInsets.only(top: 32.0, right: 0.0, bottom: 16.0),
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 8, bottom: 8),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ExpandableListViewScrollable(
                            editable: widget.role == 'admin',
                            itemsList: doctorId.isEmpty
                                ? CPAPDataPool.of(context).patientList
                                : doctorId == 'all'
                                    ? CPAPDataPool.of(context).patientList
                                    : filterData(
                                        CPAPDataPool.of(context).patientList,
                                        {'doctor_id': doctorId}),
                            editItem: editPatient,
                            deleteItem: deletePatient,
                            expandButtonImage: getAvatarImage,
                            itemPrimaryWidget: itemPrimaryInfoWidget,
                            itemSecondaryWidget: itemSecondaryInfoWidget,
                            childItemCount: getItemChildCount,
                            childItemBuilder: getItemChildInfo)
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        // floatingActionButton: addingButton(context, newPatient),
        // floatingActionButtonLocation: FloatingActionButtonLocation.startFloat,
      ),
    );
  }
}
