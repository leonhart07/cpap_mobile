import 'package:cpap_mobile/app/ui/modules/authenticated/home/home.viewmodel.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/patient/edit/edit.patient.history.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/patient/edit/edit.patient.info.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/patient/edit/edit.patient.status.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class EditPatientPage extends StatefulWidget {
  const EditPatientPage({Key key, this.patient, this.doctors})
      : super(key: key);
  final dynamic patient;
  final Map<String, Object> doctors;
  @override
  _EditPatientPageState createState() => _EditPatientPageState();
}

class _EditPatientPageState extends State<EditPatientPage> {
  final vm = inject<HomeViewModel>();
  dynamic patient;
  PageController _controller = PageController(
    initialPage: 1,
  );

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: PageView(
        controller: _controller,
        children: [
          EditPatientHistoryPage(patient: widget.patient),
          EditPatientStatusPage(patient: widget.patient),
          EditPatientInfoPage(patient: widget.patient, doctors: widget.doctors)
        ],
      ),
      floatingActionButton: SmoothPageIndicator(
        controller: _controller,
        count: 3,
        effect: JumpingDotEffect(
            activeDotColor: Colors.red, dotColor: Colors.amber),
      ),
    ));
  }
}
