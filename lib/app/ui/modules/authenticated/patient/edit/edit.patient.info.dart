import 'dart:convert';
import 'package:cpap_mobile/app/data/patient.repository.dart';
import 'package:cpap_mobile/app/ui/components/input_widget.dart';
import 'package:cpap_mobile/app/ui/components/loading.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/home/home.viewmodel.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/patient/edit/edit.patient.notifier.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EditPatientInfoPage extends StatefulWidget {
  const EditPatientInfoPage({Key key, this.patient, this.doctors})
      : super(key: key);
  final dynamic patient;
  final Map<String, Object> doctors;
  @override
  _EditPatientInfoPageState createState() => _EditPatientInfoPageState();
}

class _EditPatientInfoPageState extends State<EditPatientInfoPage> {
  dynamic patient;

  @override
  void initState() {
    patient = json.decode(json.encode(widget.patient));
    super.initState();
  }

  void updatePatient(BuildContext context) {
    final vm = inject<HomeViewModel>();
    vm
        .editPatient(patient, PatientEditType.Info)
        .then((value) => {Navigator.pop(context, value)});
  }

  Widget editPatientAction(BuildContext context) {
    bool enable = context.watch<EditPatientChangeNotifier>().infoValid;
    return MaterialButton(
      onPressed: () {
        if (enable) updatePatient(context);
      },
      child: Text(
        'Lưu',
        style: TextStyle(color: enable ? Colors.blue : Colors.grey),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (_) => EditPatientChangeNotifier(),
          ),
        ],
        child: StreamBuilder(
          builder: (context, snapshot) {
            return LoadingWidget(
              message: '',
              status: snapshot.data,
              child: SafeArea(
                child: Scaffold(
                  resizeToAvoidBottomInset: false,
                  appBar: AppBar(
                    titleSpacing: 0,
                    centerTitle: true,
                    backgroundColor: Colors.white,
                    title: pageTitleWidget(context, 'Thông Tin',
                        height: 20,
                        additionWiget: [
                          Text(
                            widget.patient['fullname'],
                            style: pageTitleTextStyle(color: Colors.red),
                            textAlign: TextAlign.center,
                          )
                        ]),
                    actions: [editPatientAction(context)],
                  ),
                  backgroundColor: Colors.white,
                  body: editPageBody(context, patient),
                ),
              ),
            );
          },
        ));
  }

  Widget editPageBody(BuildContext context, dynamic object) {
    void validateInput() {
      context
          .read<EditPatientChangeNotifier>()
          .checkInfo(patient, widget.patient);
    }

    return Container(
      width: MediaQuery.maybeOf(context).size.width,
      height: MediaQuery.maybeOf(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/background/main_light.jpg'),
          fit: BoxFit.cover,
        ),
      ),
      padding: EdgeInsets.all(12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 16),
          createTextWidget(context, 'Email', patient['email'], (value) {
            patient['email'] = value;
            validateInput();
          }, iconName: 'ic_email.svg'),
          SizedBox(height: 16),
          createTextWidget(context, 'Họ Và Tên', patient['fullname'], (value) {
            patient['fullname'] = value;
            validateInput();
          }, iconName: 'ic_patient.svg'),
          SizedBox(height: 16),
          createTextWidget(context, 'Số Điện Thoại', patient['phone_number'],
              (value) {
            patient['phone_number'] = value;
            validateInput();
          }, iconName: 'ic_phone.svg', isNumber: true),
          SizedBox(height: 16),
          createDateTimeWidget(context, 'Sinh Nhật', patient['birthday'],
              (value) {
            patient['birthday'] = value;
            validateInput();
          }),
          SizedBox(
            height: 16,
          ),
          createTextWidget(context, 'Địa chỉ', patient['address_str'], (value) {
            patient['address_str'] = value;
            validateInput();
          }, iconName: 'ic_location.svg'),
          SizedBox(
            height: 16,
          ),
          createListWidget(context, 'Bác Sĩ', widget.doctors, (value) {
            patient['doctor_id'] = value;
            validateInput();
          }, initValue: patient['doctor_id']),
        ],
      ),
    );
  }
}
