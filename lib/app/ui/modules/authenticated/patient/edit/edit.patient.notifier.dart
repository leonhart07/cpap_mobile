import 'package:flutter/material.dart';

class EditPatientChangeNotifier with ChangeNotifier {
  bool _historyValid = false;
  bool _statusValid = false;
  bool _infoValid = false;
  bool get historyValid => _historyValid;
  bool get statusValid => _statusValid;
  bool get infoValid => _infoValid;

  void checkInfo(dynamic current, dynamic origin) {
    List<String> keys = [
      "doctor_id",
      "fullname",
      "phone_number",
      "address_str",
      "email"
    ];
    _infoValid = false;
    if (current['phone_number'].length >= 10 &&
        current['fullname'].length > 0) {
      _infoValid = true;
      for (var i = 0; i < keys.length; i++) {
        if (current[keys[i]] != origin[keys[i]]) {
          _infoValid = true;
          break;
        }
      }
    }
    notifyListeners();
  }

  void checkHistory(dynamic current, dynamic origin) {
    var left = [];
    var right = [];

    if (current.containsKey('history') && origin.containsKey('history')) {
      left = current['history'] as List;
      right = origin['history'] as List;
    }
    _historyValid = false;
    for (var i = 0; i < left.length; i++) {
      if ((left[i]['date_str'] != null && left[i]['date_str'].isEmpty) ||
          (left[i]['content'] != null && left[i]['content'].isEmpty)) {
        notifyListeners();
        return;
      }
    }
    if (Set.from(left) != Set.from(right)) _historyValid = true;
    notifyListeners();
  }

  void checkStatus(dynamic current, dynamic origin) {
    var left = [];
    var right = [];

    if (current.containsKey('status') && origin.containsKey('status')) {
      left = current['status'] as List;
      right = origin['status'] as List;
    }
    _statusValid = false;
    for (var i = 0; i < left.length; i++) {
      final dateValid =
          left[i]['date_str'] != null && left[i]['date_str'].isNotEmpty;
      final contentValid =
          left[i]['content'] != null && left[i]['content'].isNotEmpty;
      if (dateValid != contentValid) {
        notifyListeners();
        return;
      }
    }
    if (Set.from(left) != Set.from(right)) {
      _statusValid = true;
    }
    notifyListeners();
  }
}
