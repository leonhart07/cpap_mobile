import 'dart:convert';

import 'package:cpap_mobile/app/data/patient.repository.dart';
import 'package:cpap_mobile/app/ui/components/input_widget.dart';
import 'package:cpap_mobile/app/ui/components/loading.dart';
import 'package:cpap_mobile/app/ui/components/snackbar.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/home/home.viewmodel.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/patient/edit/edit.patient.notifier.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class EditPatientHistoryPage extends StatefulWidget {
  const EditPatientHistoryPage({Key key, this.patient}) : super(key: key);
  final dynamic patient;
  @override
  _EditPatientHistoryPageState createState() => _EditPatientHistoryPageState();
}

class _EditPatientHistoryPageState extends State<EditPatientHistoryPage> {
  final vm = inject<HomeViewModel>();
  dynamic patient;

  @override
  void initState() {
    patient = json.decode(json.encode(widget.patient));
    sortHistoryList(patient);
    super.initState();
  }

  int sortHistoryList(Map<String, dynamic> object) {
    var statusList = object['history'] as List<dynamic>;
    if (statusList != null && statusList.length > 0) {
      if (statusList.length > 0)
        statusList.sort((a, b) {
          var date1 =
              DateFormat('dd/MM/yyyy').parse(a['date_str'] ?? '01/01/1970');
          var date2 =
              DateFormat('dd/MM/yyyy').parse(b['date_str'] ?? '01/01/1970');
          return date2.compareTo(date1);
        });
      return statusList.length;
    }
    return 0;
  }

  Future<void> newHistory(BuildContext context) async {
    sortHistoryList(patient);
    DateTime lastest = DateTime.now();
    if (patient['history'].length > 0) {
      try {
        lastest =
            DateFormat('dd/MM/yyyy').parse(patient['history'][0]['date_str']);
      } catch (e) {
        lastest = DateTime.now();
      }
      if (patient['history'][0]['date_str'].isEmpty ||
          patient['history'][0]['content'].isEmpty) return;
    }
    setState(() {
      patient['history'].insert(0, {
        'content': '',
        'date_str':
            DateFormat('dd/MM/yyyy').format(lastest.add(Duration(days: 1)))
      });
      context
          .read<EditPatientChangeNotifier>()
          .checkHistory(patient, widget.patient);
    });
  }

  void updatePatient(BuildContext context) {
    vm
        .editPatient(patient, PatientEditType.History)
        .then((value) => {Navigator.pop(context, value)});
  }

  Widget editPatientAction(BuildContext context) {
    bool enable = context.watch<EditPatientChangeNotifier>().historyValid;
    return MaterialButton(
      onPressed: () {
        if (enable) updatePatient(context);
      },
      child: Text(
        'Lưu',
        style: TextStyle(color: enable ? Colors.blue : Colors.grey),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (_) => EditPatientChangeNotifier(),
          ),
        ],
        child: StreamBuilder(
          builder: (context, snapshot) {
            return LoadingWidget(
              message: '',
              status: snapshot.data,
              child: SafeArea(
                child: Scaffold(
                  resizeToAvoidBottomInset: false,
                  appBar: AppBar(
                    titleSpacing: 0,
                    centerTitle: true,
                    backgroundColor: Colors.white,
                    title: pageTitleWidget(context, 'Lịch Sử',
                        height: 20,
                        additionWiget: [
                          Text(
                            widget.patient['fullname'],
                            style: pageTitleTextStyle(color: Colors.red),
                            textAlign: TextAlign.center,
                          )
                        ]),
                    actions: [editPatientAction(context)],
                  ),
                  backgroundColor: Colors.white,
                  body: editPageBody(context, patient),
                  floatingActionButton: addingButton(context, newHistory),
                  floatingActionButtonLocation:
                      FloatingActionButtonLocation.startFloat,
                ),
              ),
            );
          },
        ));
  }

  Widget editElement(BuildContext context, dynamic object, int index) {
    void validateInput() {
      context
          .read<EditPatientChangeNotifier>()
          .checkHistory(object, widget.patient);
    }

    dynamic item = object['history'];
    final String message = 'Removing data on ${item[index]['date_str']}';
    double height = MediaQuery.maybeOf(context).size.height;
    double width = MediaQuery.maybeOf(context).size.width;
    return Dismissible(
        // Show a red background as the item is swiped away.
        background: Container(color: Colors.grey),
        key: Key(item[index]['date_str']),
        direction: DismissDirection.endToStart,
        onDismissed: (direction) {
          if (DismissDirection.endToStart == direction)
            setState(() {
              item.removeAt(index);
              validateInput();
              ScaffoldMessenger.of(context).showSnackBar(genericSnackbar(
                  context, message,
                  background: Colors.orangeAccent[200]));
            });
        },
        child: Container(
            margin: const EdgeInsets.only(
                bottom: 6.0), //Same as `blurRadius` i guess
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.blueAccent,
                  offset: Offset(0.5, 1.0), //(x,y)
                  blurRadius: 1.0,
                ),
              ],
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                createDateTimeWidget(context, 'Ngày', item[index]['date_str'],
                    (value) {
                  item[index]['date_str'] = value;
                  validateInput();
                },
                    radius: 0.0,
                    iconName: '',
                    width: (width - 64) / 3,
                    height: height / 15,
                    passYear: 0,
                    helpText: 'Ngày Hẹn'),
                createTextWidget(context, 'Nội dung', item[index]['content'],
                    (value) {
                  item[index]['content'] = value;
                  validateInput();
                },
                    radius: 0.0,
                    iconName: '',
                    width: (width - 64) / 3 * 2,
                    height: height / 15)
              ],
            )));
  }

  Widget editPageBody(BuildContext context, dynamic object) {
    if (!object.containsKey('history') || object['history'] == null)
      object['history'] = [];
    return Stack(
      children: [
        Container(
          width: MediaQuery.maybeOf(context).size.width,
          height: MediaQuery.maybeOf(context).size.height,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/background/main_light.jpg'),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Padding(
            padding: const EdgeInsets.only(
                top: 32.0, left: 4.0, right: 0.0, bottom: 16.0),
            child: Container(
              child: Padding(
                padding: const EdgeInsets.only(
                    top: 8, left: 8, bottom: 8, right: 8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: ListView.builder(
                        itemCount: object['history'].length,
                        itemBuilder: (context, index) {
                          return editElement(context, object, index);
                        },
                      ),
                    )
                  ],
                ),
              ),
            )),
      ],
    );
  }
}
