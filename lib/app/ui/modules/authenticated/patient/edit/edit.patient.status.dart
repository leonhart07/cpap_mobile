import 'dart:convert';
import 'package:cpap_mobile/app/data/patient.repository.dart';
import 'package:cpap_mobile/app/ui/components/input_widget.dart';
import 'package:cpap_mobile/app/ui/components/loading.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/home/home.viewmodel.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/patient/edit/edit.patient.notifier.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EditPatientStatusPage extends StatefulWidget {
  const EditPatientStatusPage({Key key, this.patient}) : super(key: key);
  final dynamic patient;
  @override
  _EditPatientStatusPageState createState() => _EditPatientStatusPageState();
}

class _EditPatientStatusPageState extends State<EditPatientStatusPage> {
  final vm = inject<HomeViewModel>();
  dynamic patient;

  @override
  void initState() {
    patient = json.decode(json.encode(widget.patient));
    super.initState();
  }

  void updatePatient(BuildContext context) {
    vm
        .editPatient(patient, PatientEditType.Status)
        .then((value) => {Navigator.pop(context, value)});
  }

  Widget editPatientAction(BuildContext context) {
    bool enable = context.watch<EditPatientChangeNotifier>().statusValid;
    return MaterialButton(
      onPressed: () {
        if (enable) updatePatient(context);
      },
      child: Text(
        'Lưu',
        style: TextStyle(color: enable ? Colors.blue : Colors.grey),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (_) => EditPatientChangeNotifier(),
          ),
        ],
        child: StreamBuilder(
          builder: (context, snapshot) {
            return LoadingWidget(
              message: '',
              status: snapshot.data,
              child: SafeArea(
                child: Scaffold(
                    resizeToAvoidBottomInset: false,
                    appBar: AppBar(
                      titleSpacing: 0,
                      centerTitle: true,
                      backgroundColor: Colors.white,
                      title: pageTitleWidget(context, 'Trạng Thái',
                          height: 20,
                          additionWiget: [
                            Text(
                              widget.patient['fullname'],
                              style: pageTitleTextStyle(color: Colors.red),
                              textAlign: TextAlign.center,
                            )
                          ]),
                      actions: [editPatientAction(context)],
                    ),
                    backgroundColor: Colors.white,
                    body: editPageBody(context, patient)),
              ),
            );
          },
        ));
  }

  Widget editElement(BuildContext context, dynamic object, int index) {
    void validateInput() {
      context
          .read<EditPatientChangeNotifier>()
          .checkStatus(object, widget.patient);
    }

    dynamic item = object['status'];
    double height = MediaQuery.maybeOf(context).size.height;
    double width = MediaQuery.maybeOf(context).size.width;
    return Container(
        margin:
            const EdgeInsets.only(bottom: 6.0), //Same as `blurRadius` i guess
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.blueAccent,
              offset: Offset(0.5, 1.0), //(x,y)
              blurRadius: 1.0,
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CircleAvatar(
                radius: 10,
                backgroundColor: Colors.orange,
                child: Text('${item[index]['stat_id'] + 1}',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 10,
                        fontWeight: FontWeight.bold))),
            createDateTimeWidget(context, 'Ngày', item[index]['date_str'],
                (value) {
              item[index]['date_str'] = value;
              validateInput();
            },
                radius: 0.0,
                iconName: '',
                width: (width - 64) / 3,
                height: height / 15,
                passYear: 0,
                helpText: 'Ngày'),
            createTextWidget(context, 'Nội dung', item[index]['content'],
                (value) {
              item[index]['content'] = value;
              validateInput();
            },
                radius: 0.0,
                iconName: '',
                width: (width - 64) / 3 * 2,
                height: height / 15)
          ],
        ));
  }

  Widget editPageBody(BuildContext context, dynamic object) {
    return Stack(
      children: [
        Container(
          width: MediaQuery.maybeOf(context).size.width,
          height: MediaQuery.maybeOf(context).size.height,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/background/main_light.jpg'),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Padding(
            padding: const EdgeInsets.only(
                top: 32.0, left: 4.0, right: 0.0, bottom: 16.0),
            child: Container(
              child: Padding(
                padding: const EdgeInsets.only(
                    top: 8, left: 8, bottom: 8, right: 8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: ListView.builder(
                        itemCount: object['status'].length,
                        itemBuilder: (context, index) {
                          return editElement(context, object, index);
                        },
                      ),
                    )
                  ],
                ),
              ),
            )),
      ],
    );
  }
}
