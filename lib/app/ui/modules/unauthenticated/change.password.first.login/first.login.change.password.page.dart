import 'package:cpap_mobile/app/data/sources/cache/storage.helper.dart';
import 'package:cpap_mobile/app/data/sources/remote/app.service.dart';
import 'package:cpap_mobile/app/data/structure/common.dart';
import 'package:cpap_mobile/app/ui/components/input_widget.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/home/home.viewmodel.dart';
import 'package:cpap_mobile/app/ui/modules/unauthenticated/change.password.first.login/first.login.change.password.notifier.dart';
import 'package:cpap_mobile/app/ui/modules/unauthenticated/login/login.page.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

class FirstLoginChangePasswordPage extends StatelessWidget {
  FirstLoginChangePasswordPage({Key key}) : super(key: key);
  final vm = inject<HomeViewModel>();
  final passChanger = ChangePassword();

  void actionChangePassword(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());
    vm
        .changePassword(passChanger.oldPassword, passChanger.newPassword)
        .then((res) async {
      if (res.statusCode == 200) {
        // reset local storage
        await AppService.clearForLogout();
        // navigate to login page
        String email = await StorageHelper.get(StorageKeys.login);
        String password = await StorageHelper.get(StorageKeys.password);
        Get.to(() => LoginPage(email: email, password: password));
      } else {
        Fluttertoast.showToast(
            msg: res.data['message'],
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    });
  }

  Widget saveButtonWidget(BuildContext context) {
    bool enable = context.watch<FirstLoginChangePasswordNotifier>().enable;
    return MaterialButton(
      onPressed: () {
        if (enable) actionChangePassword(context);
      },
      child: Text(
        'Lưu',
        style: TextStyle(color: enable ? Colors.blue : Colors.grey),
      ),
    );
  }

  Widget form(BuildContext context) {
    void validateInput() {
      context.read<FirstLoginChangePasswordNotifier>().checkEnable(passChanger);
    }

    return Container(
      width: MediaQuery.maybeOf(context).size.width,
      height: MediaQuery.maybeOf(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/background/main_light.jpg'),
          fit: BoxFit.cover,
        ),
      ),
      padding: EdgeInsets.all(12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 16),
          createTextWidget(context, 'Mật Khẩu Cũ', passChanger.oldPassword,
              (value) {
            passChanger.oldPassword = value;
            validateInput();
          }, isPassword: true, iconName: 'ic_password.svg'),
          createTextWidget(context, 'Mật Khẩu Mới', passChanger.newPassword,
              (value) {
            passChanger.newPassword = value;
            validateInput();
          }, isPassword: true, iconName: 'ic_password.svg'),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => FirstLoginChangePasswordNotifier(),
        ),
      ],
      child: SafeArea(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            titleSpacing: 0,
            centerTitle: true,
            backgroundColor: Colors.white,
            title: pageTitleWidget(context, 'Đổi Mật Khẩu', height: 20),
            actions: [saveButtonWidget(context)],
          ),
          backgroundColor: Colors.white,
          body: form(context),
        ),
      ),
    );
  }
}
