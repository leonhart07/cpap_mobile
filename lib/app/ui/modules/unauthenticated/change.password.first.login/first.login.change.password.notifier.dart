import 'package:cpap_mobile/app/data/structure/common.dart';
import 'package:cpap_mobile/app/data/structure/util.dart';
import 'package:flutter/material.dart';

class FirstLoginChangePasswordNotifier with ChangeNotifier {
  bool _enable = false;
  bool get enable => _enable;

  void checkEnable(ChangePassword passChanger) {
    _enable = false;
    var oldPassValid = validatePassword(passChanger.oldPassword);
    var newPassValid = validatePassword(passChanger.newPassword);
    if (oldPassValid == null && newPassValid == null) _enable = true;
    notifyListeners();
  }
}
