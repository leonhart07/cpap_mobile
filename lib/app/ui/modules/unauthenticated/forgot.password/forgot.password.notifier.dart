import 'package:cpap_mobile/app/data/structure/util.dart';
import 'package:flutter/material.dart';

class EmailChangeNotifier with ChangeNotifier {
  bool _enable = false;
  bool get enable => _enable;

  void checkEnable(String email) {
    _enable = false;
    var isValid = validateEmail(email);
    if (isValid == null) _enable = true;
    notifyListeners();
  }
}
