import 'package:cpap_mobile/app/ui/components/input_widget.dart';
import 'package:cpap_mobile/app/ui/components/snackbar.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/home/home.viewmodel.dart';
import 'package:cpap_mobile/app/ui/modules/unauthenticated/forgot.password/forgot.password.notifier.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ForgotPasswordPage extends StatelessWidget {
  ForgotPasswordPage({Key key}) : super(key: key);
  final vm = inject<HomeViewModel>();
  var email;

  void actionFunction(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());
    vm.forgotPassword(email).then((res) async {
      Navigator.pop(context);
      if (res.statusCode == 200) {
        Future.delayed(Duration(milliseconds: 100)).then((value) {
          showTopSnackbar(
              context, 'Success', 'Email sent, please check your mailbox',
              background: Colors.greenAccent[200],
              duration: 5,
              titleColor: Colors.green);
        });
      } else {
        Future.delayed(Duration(milliseconds: 100)).then((value) {
          showTopSnackbar(
              context, 'Error ${res.statusCode}', res.data['message'],
              background: Colors.red[200], duration: 5);
        });
      }
    });
  }

  Widget actionButtonWidget(BuildContext context, bool enable) {
    return MaterialButton(
      onPressed: () {
        if (enable) actionFunction(context);
      },
      child: Text(
        'Gửi',
        style: TextStyle(color: enable ? Colors.blue : Colors.grey),
      ),
    );
  }

  Widget form(BuildContext context) {
    void validateInput() {
      context.read<EmailChangeNotifier>().checkEnable(email);
    }

    return Container(
      width: MediaQuery.maybeOf(context).size.width,
      height: MediaQuery.maybeOf(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/background/login.jpg'),
          fit: BoxFit.cover,
        ),
      ),
      padding: EdgeInsets.all(12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            child: SizedBox(
              width: 86,
              height: 86,
              child: Image.asset('assets/ic_app.png'),
            ),
          ),
          SizedBox(height: 16),
          createTextWidget(context, 'Email', email, (value) {
            email = value;
            validateInput();
          }, iconName: 'ic_email.svg'),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (_) => EmailChangeNotifier(),
          ),
        ],
        builder: (context, child) {
          bool enable = context.watch<EmailChangeNotifier>().enable;
          return SafeArea(
            child: Scaffold(
              resizeToAvoidBottomInset: false,
              appBar: AppBar(
                titleSpacing: 0,
                centerTitle: true,
                backgroundColor: Colors.white,
                title: pageTitleWidget(context, 'Đặt Lại Mật Khẩu', height: 20),
                actions: [actionButtonWidget(context, enable)],
              ),
              backgroundColor: Colors.white,
              body: form(context),
            ),
          );
        });
  }
}
