import 'package:cpap_mobile/app/data/sources/cache/storage.helper.dart';
import 'package:cpap_mobile/app/data/sources/remote/common/http_response.dart';
import 'package:cpap_mobile/app/data/user.repository.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:flutter/services.dart';
import 'package:platform_device_id/platform_device_id.dart';

class LoginViewModel {
  UserRepository repository = inject<UserRepository>();

  Future<HttpResponse> signIn(String username, String password) async {
    return await repository.login(username, password);
  }

  // Future<HttpResponse> checkToken(String token) async {
  //   return await repository.checkToken(token);
  // }

  Future<void> initPlatformState() async {
    String deviceId;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      deviceId = await PlatformDeviceId.getDeviceId;
      StorageHelper.set(StorageKeys.deviceId, deviceId);
    } on PlatformException {
      deviceId = 'Failed to get deviceId.';
    }
  }
}
