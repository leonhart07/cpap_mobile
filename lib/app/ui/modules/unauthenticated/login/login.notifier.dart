import 'package:cpap_mobile/app/data/structure/common.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class LoginNotifier with ChangeNotifier, DiagnosticableTreeMixin {
  LoginNotifier({this.isEnable = false});
  bool isEnable;
  bool get enable => isEnable;
  void checkEnable(Login account) {
    isEnable = ((account.email.isNotEmpty) && (account.password.isNotEmpty));
    notifyListeners();
  }
}
