import 'package:cpap_mobile/app/data/user.repository.dart';
import 'package:cpap_mobile/app/data/structure/common.dart';
import 'package:cpap_mobile/app/ui/components/input_widget.dart';
import 'package:cpap_mobile/app/ui/components/loading.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/home/home.page.dart';
import 'package:cpap_mobile/app/ui/modules/unauthenticated/change.password.first.login/first.login.change.password.page.dart';
import 'package:cpap_mobile/app/ui/modules/unauthenticated/forgot.password/forgot.password.page.dart';
import 'package:cpap_mobile/app/ui/modules/unauthenticated/login/login.notifier.dart';
import 'package:cpap_mobile/app/ui/modules/unauthenticated/login/login.viewmodel.dart';
import 'package:cpap_mobile/core/di/injector_provider.dart';
import 'package:cpap_mobile/device/nav/nav_slide_from_top.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:package_info/package_info.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  final String email;
  final String password;
  LoginPage({
    this.email = '',
    this.password = '',
  });

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final vm = inject<LoginViewModel>();
  UserRepository repository = inject<UserRepository>();
  PackageInfo _packageInfo = PackageInfo(
    appName: '',
    packageName: '',
    version: '',
    buildNumber: '',
  );
  Login loginInfo = Login();

  @override
  void initState() {
    loginInfo.email = widget.email;
    loginInfo.password = widget.password;
    vm.initPlatformState();
    _initPackageInfo();
    super.initState();
  }

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    print('version: ' + info.version);
    print('buildNumber: ' + info.buildNumber);
    setState(() {
      _packageInfo = info;
    });
  }

  void _unknowError() {
    Fluttertoast.showToast(
      msg: 'unknow error',
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.red,
      textColor: Colors.white,
      fontSize: 16.0,
    );
    //setState(() {});
  }

  void _login() async {
    final ret = await vm.signIn(loginInfo.email, loginInfo.password);
    if (ret.statusCode == 200) {
      Map<String, dynamic> dataJson = ret.data;
      if (dataJson.containsKey('data')) {
        Map<String, dynamic> data = dataJson['data'];
        if (data.containsKey('first_login')) {
          if (data['first_login'] == true) {
            Navigator.push(
              context,
              NavSlideFromTop(
                page: FirstLoginChangePasswordPage(),
              ),
            );
            return;
          }
        }

        if (data != null) {
          Map<String, dynamic> profile = data;
          final UserProfile userProfile = UserProfile.fromJson(profile);
          if (userProfile.authenToken.isNotEmpty) {
            Navigator.pushReplacement(
              context,
              NavSlideFromTop(
                page: HomePage(),
              ),
            );
            return;
          } else {
            _unknowError();
          }
        } else {
          _unknowError();
        }
      } else {
        _unknowError();
      }
    } else {
      Fluttertoast.showToast(
        msg: ret.message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0,
      );
    }
  }

  void _navForgotPassword() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ForgotPasswordPage(),
      ),
    );
  }

  Widget _forgotPassword() {
    return Align(
      alignment: Alignment.bottomRight,
      child: TextButton(
        onPressed: () => _navForgotPassword(),
        child: Text(
          'Quên mật khẩu',
          style: TextStyle(
            color: Colors.black,
            decoration: TextDecoration.underline,
          ),
        ),
      ),
    );
  }

  Widget _loginButton(BuildContext context) {
    bool isEnable = context.watch<LoginNotifier>().enable;
    return Container(
      width: 281,
      height: 46,
      child: Material(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(23.0),
        ),
        clipBehavior: Clip.antiAlias,
        child: MaterialButton(
          color: isEnable ? Colors.red[300] : Colors.white,
          minWidth: MediaQuery.maybeOf(context).size.width,
          padding: EdgeInsets.only(left: 10),
          child: Text(
            'Đăng nhập',
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          onPressed: isEnable ? () => _login() : null,
        ),
      ),
    );
  }

  Widget _version() {
    return Positioned(
      top: 4,
      right: 8,
      child: Text(
        'Version ' +
            _packageInfo.version +
            '(' +
            _packageInfo.buildNumber +
            ')',
        style: TextStyle(
          color: Colors.black,
          fontSize: 11,
          decoration: TextDecoration.underline,
          decorationStyle: TextDecorationStyle.solid,
        ),
        textAlign: TextAlign.end,
      ),
    );
  }

  Widget _background() {
    return Positioned(
      top: 0,
      child: Container(
        width: MediaQuery.maybeOf(context).size.width,
        height: MediaQuery.maybeOf(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/background/login.jpg'),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }

  Widget appLogo() {
    return Container(
      child: SizedBox(
        width: 86,
        height: 86,
        child: Image.asset('assets/ic_app.png'),
      ),
    );
  }

  Widget form(BuildContext context) {
    void validateInput() {
      setState(() {
        context.read<LoginNotifier>().checkEnable(loginInfo);
      });
    }

    return Stack(
      children: [
        _background(),
        _version(),
        Container(
          width: MediaQuery.maybeOf(context).size.width,
          height: MediaQuery.maybeOf(context).size.height,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 80),
              appLogo(),
              SizedBox(height: 16),
              createTextWidget(context, 'Email', loginInfo.email, (value) {
                loginInfo.email = value;
                validateInput();
              }, iconName: 'ic_email.svg'),
              SizedBox(height: 16),
              createTextWidget(context, 'Password', loginInfo.password,
                  (value) {
                loginInfo.password = value;
                validateInput();
              },
                  isPassword: true,
                  passwordVisible: loginInfo.password != widget.password,
                  iconName: 'ic_password.svg'),
              SizedBox(height: 16),
              SizedBox(height: 32),
              _loginButton(context),
              _forgotPassword(),
              SizedBox(
                height: 24,
              )
            ],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (_) => LoginNotifier(
                isEnable: ((loginInfo.email.isNotEmpty) &&
                    (loginInfo.password.isNotEmpty))),
          ),
        ],
        child: StreamBuilder(builder: (context, snapshot) {
          return LoadingWidget(
            message: '',
            status: snapshot.data,
            child: SafeArea(
              child: Scaffold(
                resizeToAvoidBottomInset: false,
                body: Container(
                  child: form(context),
                ),
              ),
            ),
          );
        }));
  }
}
