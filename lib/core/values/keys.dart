final String statusCodeKey = 'status_code';
final String usernameKey = 'username';
final String passwordKey = 'password';
final String deviceIdKey = 'device_id';
final String dataKey = 'data';
final String authenTokenKey = 'authen_token';
final String firstLoginKey = 'first_login';
final String profileKey = 'profile';
final String messageKey = 'message';

// message
final String successMessage = 'Success';
