import 'package:cpap_mobile/app/ui/modules/authenticated/home/home.page.dart';
import 'package:cpap_mobile/core/values/theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:overlay_support/overlay_support.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) => OverlaySupport(
      child: GetMaterialApp(
          debugShowCheckedModeBanner: false,
          theme: appTheme,
          home: HomePage()));
}
