import 'package:cpap_mobile/app/data/account.repository.dart';
import 'package:cpap_mobile/app/data/equipment.repository.dart';
import 'package:cpap_mobile/app/data/patient.repository.dart';
import 'package:cpap_mobile/app/data/push.notification.repository.dart';
import 'package:cpap_mobile/app/data/sources/remote/http.account.service.dart';
import 'package:cpap_mobile/app/data/sources/remote/http.equipment.service.dart';
import 'package:cpap_mobile/app/data/sources/remote/http.patient.service.dart';
import 'package:cpap_mobile/app/data/sources/remote/http.push.notification.service.dart';
import 'package:cpap_mobile/app/data/sources/remote/http.user.service.dart';
import 'package:cpap_mobile/app/data/user.repository.dart';
import 'package:cpap_mobile/app/ui/modules/authenticated/home/home.viewmodel.dart';
import 'package:cpap_mobile/app/ui/modules/unauthenticated/login/login.viewmodel.dart';
import 'package:get_it/get_it.dart';

import 'http_client.dart';

final GetIt inject = GetIt.I;

Future<void> setupInjection() async {
//Components
  inject.registerSingleton(HttpClient());

  //Remote Services
  inject.registerFactory(() => HTTPAccountService());
  inject.registerFactory(() => HTTPEquipmentService());
  inject.registerFactory(() => HTTPPatientService());
  inject.registerFactory(() => HTTPUserService());
  inject.registerFactory(() => HTTPNotificationService());

  //Repositories
  inject.registerFactory(() => AccountRepository());
  inject.registerFactory(() => EquipmentRepository());
  inject.registerFactory(() => PatientRepository());
  inject.registerFactory(() => UserRepository());
  inject.registerFactory(() => PushNotificationRepository());

  //ViewModels
  inject.registerLazySingleton(() => LoginViewModel());
  inject.registerLazySingleton(() => HomeViewModel());
}
