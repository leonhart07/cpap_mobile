import 'package:cpap_mobile/app/data/sources/remote/base/server_url.dart';
import 'package:dio/dio.dart';
import 'package:cpap_mobile/app/data/sources/cache/storage.helper.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class HttpClient {
  Dio _client;
  Dio _postClient;

  HttpClient() {
    _client = Dio();
    _client.options.baseUrl = baseUrl;
    _client.options.connectTimeout = 5000; //5s
    _client.options.receiveTimeout = 3000;
    _client.options.contentType = 'application/json';
    _client.interceptors.add(_interceptor());
    _client.interceptors.add(PrettyDioLogger(
      requestHeader: false,
      requestBody: true,
      responseBody: false,
      responseHeader: false,
      compact: true,
      maxWidth: 90,
    ));

    _postClient = Dio();
    _postClient.options.baseUrl = baseUrl;
    _postClient.options.connectTimeout = 5000; //5s
    _postClient.options.receiveTimeout = 3000;
    _postClient.options.contentType = 'application/json';
  }

  Interceptor _interceptor() {
    return InterceptorsWrapper(onRequest: (
      RequestOptions option,
    ) async {
      final storageToken = await StorageHelper.get(StorageKeys.token);
      if (storageToken != null)
        option.headers.addAll({"Authorization": 'Bearer $storageToken'});

      return option;
    });
  }

  Future<Response> getWithParams(String path, Map<String, dynamic> params) =>
      _client.get(path, queryParameters: params);

  Future<Response> get(String url) => _client.get(url);

  Future<Response> post(String url, {dynamic body}) =>
      _postClient.post(url, data: body);

  Future<Response> put(String url, {dynamic body}) =>
      _postClient.put(url, data: body);

  Future<Response> delete(String url, {dynamic body}) =>
      _postClient.delete(url, data: body);
}
